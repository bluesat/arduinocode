//*********************************************************************************
// LIBRARIES
//*********************************************************************************

// IMPORTANT: The default SD card library defines SCL_PIN and SDA_PIN, it does not use these variables again and these conflict with the I2C library
// SD library file SD2PinMap.h must be edited to comment out all definition of SCL_PIN and SDA_PIN

#include <SPI.h>
#include <SD.h>

//*********************************************************************************
// SD CONSTANTS
//*********************************************************************************

#define CS_PIN 10 // Arduino pin connected to CS of SD card

//*********************************************************************************
// GLOBAL VARIABLES
//*********************************************************************************

Sd2Card card;
SdVolume volume;
SdFile root;

// File dataFile; // global variable for file

#define FILENAME "DATALOG.TXT" // file name to write to

#define WRITE_STATUS WRITE
#define WRITE 1
#define NO_WRITE 0
#define DEBUG_VERBOSE 1
#define EXIT_ERROR -1
#define EXIT_COMPLETE 1

#define DELAY_END 1000

#define PIN_BAT 0
#define PIN_5V 1
#define PIN_3V3 2

//*********************************************************************************
// FUNCTION DECLARATION
//*********************************************************************************

// define low level SD functions here
int writeValue(double value, String prefix, String suffix, String fileName); // writes value to SD card
int writeString(char text[], char fileName[]); // write string to the SD card

//*********************************************************************************
// CORE FUNCTIONS
//*********************************************************************************

void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);

  Serial.print("Initializing SD card...");

  // see if the card is present and can be initialized:
  if (!SD.begin(CS_PIN)) {
    Serial.println("Card failed, or not present");
    // don't do anything more:
    while (1);
  }
  Serial.println("card initialized.");

  writeString("START RUN", "data.txt");

  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);
  pinMode(A4, INPUT);
  pinMode(A5, INPUT);

  
}

// the loop function runs over and over again forever
void loop() {
  
  int pinread3V3;
  int pinreadBat;
  int pinread5V;
  
  pinread3V3 = analogRead(PIN_3V3);
  pinreadBat = analogRead(PIN_BAT);
  pinread5V = analogRead(PIN_5V);
  
  writeValue(pinread3V3, "3V3 - ", " ", "data.txt");
  writeValue(2*pinread5V, "5V - ", " ", "data.txt");
  writeValue(2*pinreadBat, "Bat - ", " ", "data.txt");

  
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(DELAY_END);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(DELAY_END);                       // wait for a second
  
}

//*********************************************************************************
// ADDITIONAL FUNCTIONS
//*********************************************************************************

// ARGUMENTS: float value to be written, strings for prefix (before the value), suffix (after the value) and filename - even if not used they should be empty strings
// RETURN VALUE: returns EXIT_COMPLETE unless error occured
int writeValue(double value, String prefix, String suffix, String fileName) {

  File dataFile = SD.open(fileName, FILE_WRITE);

  if (dataFile) {
    dataFile.print(prefix);
    dataFile.print(value);
    dataFile.println(suffix);
  }
  else {
    if (DEBUG_VERBOSE) {
      Serial.print(fileName);
      Serial.println(F(" - File does not exist for writing to"));
    }
    dataFile.close();
    return EXIT_ERROR;
  }

  dataFile.close();
  return EXIT_COMPLETE;
  
}

// helper function does all non-copy code with a flag/switch case

// PURPOSE: writes a string to the defined file on the SD card
// ARGUMENTS: string of text to be written and filename
// RETURN VALUE: returns EXIT_COMPLETE unless error occured

int writeString(char text[], char fileName[]) {

  File dataFile = SD.open(fileName, FILE_WRITE);

  if (dataFile) {
    dataFile.println(text);
  }
  else {
    if (DEBUG_VERBOSE) {
      Serial.print(fileName);
      Serial.println(F(" - File does not exist for writing to"));
    }
    dataFile.close();
    return EXIT_ERROR;
  }

  dataFile.close();
  return EXIT_COMPLETE;
  
}


