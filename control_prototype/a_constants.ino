//*********************************************************************************
// PROGRAMMING PARAMETERS
//*********************************************************************************

// constant that is used to print additional debugging information (1 for print, 0 for normal - otherwise poorly defined) 
#define DEBUG_VERBOSE 0

// used to enable/disable sleep mode on devices
#define SLEEP_MODE_OFF 0
#define SLEEP_MODE_ON 1

#define LSB_MASK 0xFF

#define KILO 1000 // converts over 1000x

//*********************************************************************************
// CHIP ADDRESSES
//*********************************************************************************

#define I2C_IO_ADDR_1 B01000000 // (0x70) 0x40 input output expander - has different base addresses TCA9554A/NOT TCA9554 - see http://www.ti.com/product/TCA9554A
#define I2C_CTRL_ADDR_1 B10000000 // 0x40 control chip I2C address with both addresses grounded

//*********************************************************************************
// I2C/DELAYS
//*********************************************************************************

// currently set for Arduino Uno
#define SCL_PIN 5
#define SDA_PIN 4
#define SCL_PORT PORTC
#define SDA_PORT PORTC

// for the i2c_read function, if argument is true then NACK bit is sent (indicates end of transfer sequence) 
#define NACK true
#define ACK false

// I2C read/write bits (last bit of I2C address)
#define WRITE 0
#define READ 1

// various delays
#define DELAY_I2C 50 // generic delay time to account for lag in milliseconds
#define DELAY_PULSE 10 // time to pulse the reset pin on the latching comparator to reset (in microseconds)
#define DELAY_END 5120 // time to update most registers (some are 5.12 seconds) in milliseconds
#define DELAY_TEST 5000 // time to wait test measurements at stable environment
#define DELAY_STABLE 500 // time to allow circuit to settle after changes made to stabilise it
#define DELAY_PRINT 1000 // gives time to print text and allow Arduino to be ready for next time sensitive command
#define DELAY_WAKE_ADC 200 // time for ADC to fully charge capacitors and fully wake in milliseconds
#define DELAY_MEASURE_ADC 5 // time to allow ADC to acquire information (in microseconds) (datasheet says 250ns is sufficient - this may be excessive)
#define DELAY_LED 500 // time to hold an LED for a status indicator
#define DELAY_CAP 7500 // time to allow the capacitors to charge before turning on

// default function exit statuses
#define EXIT_COMPLETE 1
#define EXIT_ERROR -2

//*********************************************************************************
// MEMORY LOCATIONS
//*********************************************************************************

// memory locations on the IO expander
// http://www.ti.com/lit/ds/symlink/tca9554.pdf
#define IO_REG_CONFIG B00000011 // register 3 is the configuration register
#define IO_REG_OUTPUT B00000001 // register 1 is the output pin register
#define IO_REG_INPUT B00000000 // register 0 is the input pin register
#define IO_REG_INVERT B00000010 // register 2 is the invert polarity register

#define IO_CONFIG_DEFAULT B10000000 // sets channels to outputs/inputs (1 = input and 0 = output)
#define IO_INVERT_DEFAULT B00000000 // sets all inversion bits to 0 (i.e. not inverted)

#define CTRL_REG_VOLTAGE_BUS 0x02
#define CTRL_REG_VOLTAGE_SHUNT 0x01
#define CTRL_REG_CALIBRATION 0x05
#define CTRL_REG_CURRENT 0x04
#define CTRL_REG_CONFIGURATION 0x00
#define CTRL_REG_MANF_ID 0xFE

#define CTRL_ALERT_TEST 8192 // alert pin activated on bus over voltage
#define CTRL_ALERT_DEFAULT 0 // alert pin activated on bus over voltageno alert pin activation

//*********************************************************************************
// CIRCUIT PARAMETERS
//*********************************************************************************

// control chip
#define LSB_VOLTAGE_BUS 1.25 // mV per LSB of bus voltage
#define LSB_VOLTAGE_SHUNT 2.5 // uV per LSB of shunt voltage
