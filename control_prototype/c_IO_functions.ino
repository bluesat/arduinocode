// define IO expander low level functions here

//*********************************************************************************
// FUNCTION DECLARATION
//*********************************************************************************

byte readRegisterI2C(byte addr, byte reg); // read a register from the IO expander
int writeRegisterI2C(byte addr, byte reg, byte data); // write to a register on the IO expander
int setIOChannels(byte addr); // initial set of the pin types on the IO expander
int turnPinOn(byte addr, int pin); // turn an individual pin on
int turnPinOff(byte addr, int pin); // turn an individual pin off

//*********************************************************************************
// FUNCTIONS
//*********************************************************************************

// PURPOSE: reads a register of the IO expander
// ARGUMENTS: addr = 8 bit address of chip, reg = 8 bit register to be read
// RETURN VALUE: returns the byte of the register read
byte readRegisterI2C(byte addr, byte reg){

  byte output = 0;

  i2c_start(addr|I2C_WRITE);
  if(!i2c_write(reg)) {
    Serial.println(F("ERROR - No ACK bit received from reading register in IO expander"));
    i2c_stop(); // frees the bus line
    return EXIT_ERROR;
  }

  i2c_rep_start(addr|I2C_READ);

  output = i2c_read(NACK);

  i2c_stop();
  
  return output;
 
}

// PURPOSE: writes to a register of the IO expander
// ARGUMENTS: addr = 8 bit address of chip, reg = 8 bit register to be written to, data = byte of data to be written
// RETURN VALUE: returns exit status
int writeRegisterI2C(byte addr, byte reg, byte data){

  i2c_start(addr|I2C_WRITE);
  if(!i2c_write(reg)) {
    Serial.println(F("ERROR - No ACK bit received from writing register select in IO expander"));
    i2c_stop(); // frees the bus line
    return EXIT_ERROR;
  }

  if(!i2c_write(data)) {
    Serial.println(F("ERROR - No ACK bit received from writing data in IO expander"));
    i2c_stop(); // frees the bus line
    return EXIT_ERROR;
  }

  i2c_stop();
  
  return EXIT_COMPLETE;
 
}

// PURPOSE: set the pins on the IO expander as outputs
// ARGUMENTS: addr = 8 bit address of chip
// RETURN VALUE: returns an error integer/complete integer
int setIOChannels(byte addr) {

  // set the inversion register
  writeRegisterI2C(addr, IO_REG_INVERT, IO_INVERT_DEFAULT);
  Serial.println(F("IO expander inversion byte setup"));

  // set the output register
  writeRegisterI2C(addr, IO_REG_CONFIG, IO_CONFIG_DEFAULT);
  Serial.println(F("IO configurated byte setup"));

  return EXIT_COMPLETE;
  
}

// PURPOSE: turns a single pin on and leaves the rest
// ARGUMENTS: addr = 8 bit address of chip and integer of pin to be turned on
// RETURN VALUE: returns an error integer (1 indicates successful setting of data)
int turnPinOn(byte addr, int pin){

  byte pinStatus = readRegisterI2C(addr, IO_REG_OUTPUT); // get the current outputs

  if(DEBUG_VERBOSE){
    Serial.print(F("Current Output Reg IO: "));
    Serial.println(pinStatus, BIN);
  }

  if(pin < 0 || pin > 7){
    Serial.println(F("Invalid pin selected - 0 to 7 only"));
    return EXIT_ERROR;
  }

  byte data = B00000001; // output pin settings to be written to IO expander (filled with boolean arguments)
  // or to turn a low to a high and keep high if alreadu
  data = pinStatus | (data << pin); // 1 is bitshifted over to the corresponding pin address

  if(DEBUG_VERBOSE){
    Serial.print(F("Setting Output Reg IO: "));
    Serial.println(data, BIN);
  }

  writeRegisterI2C(addr, IO_REG_OUTPUT, data);

  return EXIT_COMPLETE;

}

// PURPOSE: turns a single pin off and leaves the rest
// ARGUMENTS: addr = 8 bit address of chip and integer of pin to be turned on
// RETURN VALUE: returns an error integer (1 indicates successful setting of data)
int turnPinOff(byte addr, int pin){

  byte pinStatus = readRegisterI2C(addr, IO_REG_OUTPUT); // get the current outputs

  if(DEBUG_VERBOSE){
    Serial.print(F("Current Output Reg IO: "));
    Serial.println(pinStatus, BIN);
  }

  if(pin < 0 || pin > 7){
    Serial.println(F("Invalid pin selected - 0 to 7 only"));
    return EXIT_ERROR;
  }

  byte data = B00000001; // output pin settings to be written to IO expander (filled with boolean arguments)
  // and to turn a high to low and keep low if already
  data = pinStatus & !(data << pin); // 0 is bitshifted over to the corresponding pin address (as 0s are generated the 1 is shifted over and then inverted so all 0s become 1s)

  if(DEBUG_VERBOSE){
    Serial.print(F("Setting Output Reg IO: "));
    Serial.println(data, BIN);
  }

  writeRegisterI2C(addr, IO_REG_OUTPUT, data);

  return EXIT_COMPLETE;

}

// PURPOSE: reads the value of a single input pin
// ARGUMENTS: addr = 8 bit address of chip and integer of pin to be read
// RETURNS: false if low and true if high
boolean readPin(byte addr, int pin){

  byte pinStatus = readRegisterI2C(addr, IO_REG_INPUT); // get the current inputs

  if(DEBUG_VERBOSE){
    Serial.print(F("Full Input Reg IO: "));
    Serial.println(pinStatus, BIN);
  }

  if(pin < 0 || pin > 7){
    Serial.println(F("Invalid pin selected - 0 to 7 only"));
    return EXIT_ERROR;
  }

  byte data = B00000001; // output pin settings to be written to IO expander (filled with boolean arguments)
  // turns all pins to zero except for pin being read
  data = pinStatus & (data << pin); // 0 is bitshifted over to the corresponding pin address (as 0s are generated the 1 is shifted over and then inverted so all 0s become 1s)

  if(DEBUG_VERBOSE){
    Serial.print(F("Reduced Input Reg IO: "));
    Serial.println(data, BIN);
  }

  if(data == 0){
    return false;
  }
  else{
    return true;
  }
  
}

