// define control board low level functions here

//*********************************************************************************
// FUNCTION DECLARATION
//*********************************************************************************

int16_t readRegister16bitI2C(byte addr, byte reg); // read a register from the control chip
int writeRegister15bitI2C(byte addr, byte reg, uint16_t data); // writes 2 bytes to a register on the control chip
float getBusVoltage(byte addr); // read the bus voltage (V)
float getShuntVoltage(byte addr); // read the shunt voltage across the sense resistor (mV)

//*********************************************************************************
// FUNCTIONS
//*********************************************************************************

// PURPOSE: reads a register of the control chip
// ARGUMENTS: addr = 8 bit address of chip, reg = 8 bit register to be read
// RETURN VALUE: returns the two bytes of the register read
int16_t readRegister16bitI2C(byte addr, byte reg){

  int16_t output = 0;
  byte MSB = 0;
  byte LSB = 0;

  i2c_start(addr|I2C_WRITE);
  if(!i2c_write(reg)) {
    Serial.println(F("ERROR - No ACK bit received from reading register in control"));
    i2c_stop(); // frees the bus line
    return EXIT_ERROR;
  }

  i2c_stop();

  i2c_start(addr|I2C_READ);
  MSB = i2c_read(ACK);
  LSB = i2c_read(NACK);

  i2c_stop();

  output = (MSB << 8) | LSB;

  if(DEBUG_VERBOSE){
    Serial.print("MSB: ");
    Serial.println(MSB, BIN);
    Serial.print("LSB: ");
    Serial.println(LSB, BIN);
  }
  
  return output;
 
}

// PURPOSE: writes to a register in the control chip
// ARGUMENTS: addr = 8 bit address of chip, reg = 8 bit register to write to, data = 2 bytes of data to be written
// RETURN VALUE: returns the exit status
int writeRegister15bitI2C(byte addr, byte reg, uint16_t data){

  // breaks the 2 bytes up to individual bytes for writing
  byte MSB = data >> 8; // bit shifts the MSB to LSB position
  byte LSB = data & LSB_MASK; // masks the MSB with 0 to keep only LSB

  if(DEBUG_VERBOSE){
    Serial.print("MSB: ");
    Serial.println(MSB, BIN);
    Serial.print("LSB: ");
    Serial.println(LSB, BIN);
  }

  i2c_start(addr|I2C_WRITE);
  if(!i2c_write(reg)) {
    Serial.println(F("ERROR - No ACK bit received from writing register in control"));
    i2c_stop(); // frees the bus line
    return EXIT_ERROR;
  }

  if(!i2c_write(MSB)) {
    Serial.println(F("ERROR - No ACK bit received from writing MSB in control"));
    i2c_stop(); // frees the bus line
    return EXIT_ERROR;
  }


  if(!i2c_write(LSB)) {
    Serial.println(F("ERROR - No ACK bit received from writing LSB in control"));
    i2c_stop(); // frees the bus line
    return EXIT_ERROR;
  }

  i2c_stop();

  return EXIT_COMPLETE;
  
}

// PURPOSE: reads the bus voltage (V) from the high side
// ARGUMENTS: addr = 8 bit address of chip
// RETURN VALUE: returns the float of the voltage in V
float getBusVoltage(byte addr){

  return readRegister16bitI2C(addr, CTRL_REG_VOLTAGE_BUS) * LSB_VOLTAGE_BUS/1000.0;
  
}

// PURPOSE: reads the shunt voltage (mV) across the sense resistor
// ARGUMENTS: addr = 8 bit address of chip
// RETURN VALUE: returns the float of the voltage in mV
float getShuntVoltage(byte addr){

  return readRegister16bitI2C(addr, CTRL_REG_VOLTAGE_SHUNT) * LSB_VOLTAGE_SHUNT/1000.0;
  
}

// PURPOSE: sets the bus voltage limit on the control pin for alerts
// ARGUMENTS: addr = 8 bit address of chip, limit = voltage (in V) of bus limit
// RETURN VALUE: returns the exit status
int setVoltageBusLimit(byte addr, float limit){

  uint16_t convertedLimit = limit*KILO/LSB_VOLTAGE_BUS

  writeRegister15bitI2C(addr, 0x07, uint16_t data)  

  return EXIT_COMPLETE;
  
}

