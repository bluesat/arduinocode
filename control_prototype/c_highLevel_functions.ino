//*********************************************************************************
// FUNCTION DECLARATION
//*********************************************************************************


//*********************************************************************************
// FUNCTIONS
//*********************************************************************************

// PURPOSE: flashes an LED 3 times to indicate startup complete
int flashLight(){

  for (int i = 0; i < 5; i++){
    turnPinOff(I2C_IO_ADDR_1, 1);
    delay(100);
    turnPinOn(I2C_IO_ADDR_1, 1);
    delay(200);
  }

  return EXIT_COMPLETE;
  
}

