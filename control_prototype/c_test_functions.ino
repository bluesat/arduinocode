//*********************************************************************************
// FUNCTION DECLARATION
//*********************************************************************************

int checkIORegisters(byte addr); // prints IO registers
int checkCTRLManufacturersID(byte addr); // print manufacturers ID from control chip

//*********************************************************************************
// FUNCTIONS
//*********************************************************************************

// PURPOSE: prints the registers of the IO expander
// ARGUMENTS: addr = 8 bit address of chip
// RETURN VALUE: returns exit status
int checkIORegisters(byte addr){

  Serial.print(F("IO Input Register: "));
  Serial.println(readRegisterI2C(addr, IO_REG_INPUT), BIN);
  Serial.print(F("IO Output Register: "));
  Serial.println(readRegisterI2C(addr, IO_REG_OUTPUT), BIN);
  Serial.print(F("IO Inversion Register: "));
  Serial.println(readRegisterI2C(addr, IO_REG_INVERT), BIN);
  Serial.print(F("IO Configuration Register: "));
  Serial.println(readRegisterI2C(addr, IO_REG_CONFIG), BIN);

  return EXIT_COMPLETE;

}

// PURPOSE: prints the manufacturers ID of the chip
// ARGUMENTS: addr = 8 bit address of chip
// RETURN VALUE: returns exit status
int checkCTRLManufacturersID(byte addr){

  Serial.print(F("CTRL Manf ID: "));
  Serial.println(readRegister16bitI2C(I2C_CTRL_ADDR_1, CTRL_REG_MANF_ID), BIN);

  return EXIT_COMPLETE;
  
}

