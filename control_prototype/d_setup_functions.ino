//*********************************************************************************
// FUNCTION DECLARATION
//*********************************************************************************

int setupIO(char circuit); // sets all pins as output, sets reset/shutdown pins and resets latches

//*********************************************************************************
// FUNCTIONS
//*********************************************************************************

// PURPOSE: sets up the IO expanders over I2C
// ARGUMENTS: char circuit (usually number on board designator) referring to it
// RETURN: EXIT_COMPLETE if successful (1) and EXIT_ERROR if not (-2)
int setupIO(char circuit) {

  byte addr;

  if (circuit == '1') {
    addr = I2C_IO_ADDR_1;
  }/*
  else if (circuit == '2') {
    addr = I2C_IO_ADDR_2;
  }*/
  else {
    return EXIT_ERROR;
  }

  Serial.print(F("IO "));
  Serial.print(circuit);
  Serial.println(F(" - Setting up ..."));

  // sets all IO pins as outputs
  Serial.println(F("Setting pins as output/input"));
  if(setIOChannels(addr)!=EXIT_COMPLETE){
    Serial.println(F("Unable to setup IO outputs"));
    return EXIT_ERROR;
  }

  Serial.print(F("IO "));
  Serial.print(circuit);
  Serial.println(F(" - Setup complete"));
  
  return EXIT_COMPLETE;

}
