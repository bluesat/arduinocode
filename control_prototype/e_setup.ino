void setup() {

  Serial.begin(9600);
  Serial.println(F("Starting Program..."));

  Serial.println(F("Setting up I2C bus..."));
  if(!i2c_init()) {
    Serial.println(F("Initilisation failed - bus locked up"));
  }


  delay(DELAY_I2C);

  if(DEBUG_VERBOSE) {
    Serial.println(F("\nDefault IO registers on startup are...:"));
    checkIORegisters(I2C_IO_ADDR_1);
    delay(500);
  }

  Serial.println(F("\nSETUP - Setting up IO expanders"));
  setupIO('1');
  Serial.println(F("SETUP - IO expanders setup complete\n"));

  Serial.println(F("SETUP - Complete\n\n"));

  delay(500);

  if (DEBUG_VERBOSE) {
    Serial.println(F("\nInitial IO registers on startup are...:"));
    checkIORegisters(I2C_IO_ADDR_1);
    checkCTRLManufacturersID(I2C_CTRL_ADDR_1);
  }

  setVoltageBusLimit(I2C_CTRL_ADDR_1, 7.0);
  setAlertRegister(I2C_CTRL_ADDR_1, 'b');
  Serial.println("SETUP - Alert register set to bus voltage\n");

  flashLight(); // flash LED to indicate setup is complete

}
