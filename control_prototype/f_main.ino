void loop() {


//  delay(2000);
//
//  Serial.println("Off");
//  turnPinOn(I2C_IO_ADDR_1, 1);
//
//  digitalWrite(7, HIGH);
//  if(readPin(I2C_IO_ADDR_1, 7)){
//    Serial.println("Input HIGH");
//  }
//
//  delay(2000);
//
//  Serial.println("On");
//  turnPinOff(I2C_IO_ADDR_1, 1);
//
//  digitalWrite(7, LOW);
//  if (!(readPin(I2C_IO_ADDR_1, 7))) {
//    Serial.println("Input LOW");
//  }
//
//  delay(2000);
//
//  turnPinOn(I2C_IO_ADDR_1, 6);
//  Serial.println("On");

  delay(2000);

  delay(1000);
  Serial.print("Bus Voltage (V): ");
  Serial.println(getBusVoltage(I2C_CTRL_ADDR_1));
  Serial.print("Shunt Voltage (mV): ");
  Serial.println(getShuntVoltage(I2C_CTRL_ADDR_1));

  delay(5000);
  setVoltageBusLimit(I2C_CTRL_ADDR_1, 4.5);
  Serial.println("Voltage limit set");

  delay(5000);
  setVoltageBusLimit(I2C_CTRL_ADDR_1, 7.0);
  Serial.println("Voltage limit removed");
  
}
