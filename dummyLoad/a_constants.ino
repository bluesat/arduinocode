  // Libraries
#include <Wire.h>                             // include I2C library
#include <MCP79410_Timer.h>                   // Scullcom Hobby Electronics library  http://www.scullcom.com/MCP79410Timer-master.zip
#include <Adafruit_MCP4725.h>                 // Adafruit DAC library  https://github.com/adafruit/Adafruit_MCP4725
#include <math.h>                             // more mathematical functions
#include <MCP342x.h>                          // Steve Marple library avaiable from    https://github.com/stevemarple/MCP342x
#include <LiquidCrystal_I2C.h>                // F Malpartida's NewLiquidCrystal library
                                              // https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads/NewliquidCrystal_1.3.4.zip               
#include <Keypad.h>                           //http://playground.arduino.cc/Code/Keypad

// to change what is tested or not, change to a 0 (not tested) or 1 (tested)
#define TEST_I2C 0 // reports what I2C addresses are active on the line
#define TEST_ADC 0
#define TEST_DAC 0 // WARNING - DO NOT ACTIVATE THIS UNLESS THE MOSFETS ARE NOT CONNECTED - WARNING - THIS WILL DRIVE THE MOSFETS
#define TEST_RTC 0
#define TEST_LCD 0
#define TEST_THERMAL 0

#define CAL_DAC 0

#define KILO 1000

// LCD
//Set the pins on the I2C chip used for LCD connections
//ADDR,EN,R/W,RS,D4,D5,D6,D7
LiquidCrystal_I2C lcd(0x3F, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);    //0x3F (0b0111111) is the default address of the LCD with I2C bus module (PCF8574A)
int VoltsDecimalPlaces = 3;                   //number of decimal places used for Voltage display on LCD

// THERMAL SENSOR
#define I2C_ADD_THERMAL 0b1001101

// RTC
#define I2C_ADD_RTC 0x6f
MCP79410_Timer timer = MCP79410_Timer(I2C_ADD_RTC);
#define DELAY_RTC 1000

// DAC
Adafruit_MCP4725 dac;                         //constructor
#define I2C_ADD_DAC 0x63
#define DAC_VOLTAGE 4.102 // DAC voltage reference in V
#define DAC_BASIS 4096 // 12 bit DAC has 4096 possibilites
#define DELAY_DAC 1500

// ADC
#define I2C_ADD_ADC 0x68                       //0x68 is the default address for the MCP3426 device
MCP342x adc = MCP342x(I2C_ADD_ADC);

// KEYPAD
const byte ROWS = 4;                          //four rows
const byte COLS = 4;                          //four columns
byte rowPins[ROWS] = {5, 6, 7, 8}; //connect to the row Arduino pinouts of the keypad
byte colPins[COLS] = {9, 10, 11, 12}; //connect to the column Arduino pinouts of the keypad
//define the cymbols on the buttons of the keypads
char hexaKeys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};
Keypad customKeypad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS); //initialize an instance of class NewKeypad
char customKey;
char decimalPoint;

char numbers[20];     // keypad number entry - Plenty to store a representation of a float
byte index = 0;
int z = 0;
float x = 0;
int y = 0;
int r = 0;

// CALIBRATION

// error relationship between set current and input current (linear)
#define CURRENT_SET_OFFSET 0.0395 // A
#define CURRENT_SET_SLOPE 0.0864 // A/A

// error relationship between measured current and input current (constant)
#define CURRENT_MEASURE_OFFSET 0.041 // A

// error relationship between measured voltage and input voltage (linear)
#define VOLTAGE_ERROR_OFFSET 0.1562 // V
#define VOLTAGE_ERROR_SLOPE -0.007032 // V/V

// FAN
#define PIN_FAN 3
#define TEMP_FAN_MIN 20 // temperature at which fan will activate
int tempCutOff = 60;                             // temperature at which circuit will shut off
int tempMax = 75;                             //maximum temperature when fan speed at 100%
int fanSpeed;

// SETTINGS
String Mode ="  ";                            //used to identify which mode
int modeSelected = 0;                         //Mode status flag
float reading = 0;                            //variable for Rotary Encoder value divided by 1000 (unused in current code)
int setReading = 0;                           //
int ControlVolts = 0;                         //used to set output current
float OutputVoltage = 0;                      //

float setCurrent = 0;                         //variable used for the set current of the load
float setPower = 0;                           //variable used for the set power of the load
float setResistance = 0;                      //variable used for the set resistance of the load
float setCurrentCalibrationFactor = 1;        //calibration adjustment - set as required (was 0.997)

float setControlCurrent = 0;                  //variable used to set the temporary store for control current required

// ERRORS
float voltageOffset = 0;                      //variable to store voltage reading zero offset adjustment at switch on
float currentOffset = 0;                      //variable to store current reading zero offset adjustment at switch on

// MEASUREMENTS
unsigned long controlVoltage = 0;             //used for DAC to control MOSFET
long current = 0;                             //variable used by ADC for measuring the current
long voltage = 0;                             //variable used by ADC for measuring the voltage
float ActualVoltage = 0;                      //variable used for Actual Voltage reading of Load
float ActualCurrent = 0;                      //variable used for Actual Current reading of Load
float ActualPower = 0;                        //variable used for Actual Power reading of Load
int temp;                                     //

// CUTOFFS
float PowerCutOff = 50;                       //maximum Power allowed in Watts - then limited to this level CAN BE CHANGED AS REQUIRED
float CurrentCutOff = 4;                      //maximum Current setting allowed in Amps - then limited to this level (was 5)
float ResistorCutOff = 999;                   //maximum Resistor we want to deal with in software
float BatteryCurrent;                         //
float LoadCurrent;                            //

// ON/OFF
boolean toggle = false;                       //used for toggle of Load On/Off button
#define PIN_LOAD_ON_OFF 15                    //analog pin A1 used as a digital pin to set Load ON/OFF
