// ADC
void readVoltageCurrent (void);
void ActualReading(void);
void zeroOffset (void);

// DAC
void dacControl (void);

// LCD
void LCDStart();
void LCDI2CCheckGood();
void LCDI2CCheckBad();
void displayEncoderReading (void);

// keypad
void readKeypadInput (void);
void inputValue (void);
void userSetUp (void);

// temp
void temperatureCutOff (void);
void getTemp();

// I2C
int checkAddress(byte address);
void i2c_scanner();
int checkI2CDevices();

// ctrl
void Current(void);
void Power(void);
void Resistance(void);
void dacControlVoltage (void);
void powerLevelCutOff (void);
void maxConstantCurrentSetting (void);

