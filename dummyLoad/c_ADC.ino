//-----------------------Read Voltage and Current---------------------------------------------
void readVoltageCurrent (void) {
  
  MCP342x::Config status;
  // Initiate a conversion for voltage; convertAndRead() will wait until it can be read
  adc.convertAndRead(MCP342x::channel1, MCP342x::oneShot,
           MCP342x::resolution16, MCP342x::gain1,                         //"gain1" means we have select the input amp of the ADC to x1
           1000000, voltage, status);
  
  // Initiate a conversion for current; convertAndRead() will wait until it can be read
  adc.convertAndRead(MCP342x::channel2, MCP342x::oneShot,
           MCP342x::resolution16, MCP342x::gain4,                         //"gain4" means we have select the input amp of the ADC to x4
           1000000, current, status);
  
}

//----------------------Calculate Actual Voltage and Current and display on LCD---------------------
void ActualReading(void) {

  float VoltageError = 0.0;
  
  ActualCurrent = ((((current - currentOffset)*2.048)/32767) * 2.5) + CURRENT_MEASURE_OFFSET;        //calculate load current
  
  ActualVoltage = ((((voltage - voltageOffset)*2.048)/32767) * 50.4);       //calculate load voltage upto 100v (was 50)
  VoltageError = ActualVoltage*VOLTAGE_ERROR_SLOPE + VOLTAGE_ERROR_OFFSET;
  ActualVoltage = ActualVoltage + VoltageError;
  
  ActualPower = ActualVoltage*ActualCurrent;

  if (ActualPower <=0){
    ActualPower = 0;
  }

 if (ActualVoltage <=0.0){                              //added to prevent negative readings on LCD due to error
  ActualVoltage = 0.0;
 }
 
 if (ActualCurrent <= 0.0){                             //added to prevent negative readings on LCD due to error
  ActualCurrent = 0.0;
 }
  
 lcd.setCursor(0,1);
    
    if ( ActualCurrent < 10.0 ) {
        lcd.print(ActualCurrent,3);
    } else {
        lcd.print(ActualCurrent,2);
    }
    
    lcd.print("A");
    lcd.print(" ");
    
    if (ActualVoltage < 10.0) {
        lcd.print(ActualVoltage, 3);
    } else {
        lcd.print(ActualVoltage, 2);
    }    

    lcd.print("V");
    lcd.print(" ");
     
    if (ActualPower < 100 ) {
        lcd.print(ActualPower,2);
    } else {
        lcd.print(ActualPower,1);
    }
    lcd.print("W");
    lcd.print(" ");
}


//--------------------------Zero Setting Offset Routine--------------------------------------------
void zeroOffset (void) {

  delay(200);                                            //simple key bounce delay 
  readVoltageCurrent();                                  //routine for ADC to read actual Voltage and Current
  voltageOffset = voltage;
  currentOffset = current;

 //Serial.print("voltageOffset = ");                    //used for testing only
 //Serial.println(voltageOffset);                       //used for testing only
 //Serial.print("currentOffset = ");                    //used for testing only
 //Serial.println(currentOffset);                       //used for testing only
 
}
