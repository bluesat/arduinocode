//--------------------------Set DAC Voltage--------------------------------------------
void dacControl (void) {
  
  if (!toggle){
    dac.setVoltage(0,false);                                 //set DAC output voltage to 0 if Load Off selected
  }
  else{
    //Serial.println("Control Voltage");                    //used for testing only
    //Serial.println(controlVoltage);                       //used for testing only
    dac.setVoltage(controlVoltage,false);                   //set DAC output voltage for Range selected
  }
  
}


//-----------------------DAC Control Voltage for Mosfet---------------------------------------
void dacControlVoltage (void) {

  float controlCurrentError = 0.0;
  
  if (Mode == "CC"){
    setCurrent = reading*1000;                                //set current is equal to input value in Amps
    setReading = setCurrent;                                  //show the set current reading being used
    setControlCurrent = setCurrent * setCurrentCalibrationFactor;
  }

  if (Mode == "CP"){
    setPower = reading*1000;                                  //in Watts
    setReading = setPower;
    setCurrent = setPower/ActualVoltage;
    setControlCurrent = setCurrent * setCurrentCalibrationFactor;
  }

  if (Mode == "CR"){
    setResistance = reading;                                  //in ohms
    setReading = setResistance;
    setCurrent = (ActualVoltage)/setResistance*1000;
    setControlCurrent = setCurrent * setCurrentCalibrationFactor;
  }

  if (Mode == "TC" || Mode == "TP" || Mode == "TT"){                            //Transient Mode - Continuous
    setControlCurrent = (setCurrent * 1000) * setCurrentCalibrationFactor;
  }

  controlCurrentError = CURRENT_SET_SLOPE*setControlCurrent + CURRENT_SET_OFFSET;
  controlVoltage = setControlCurrent + controlCurrentError; 

}
