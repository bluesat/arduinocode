// prints a short starting message
void LCDStart(){
  

  // lcd.setBacklightPin(3,POSITIVE);                         // BL, BL_POL
  // lcd.setBacklight(HIGH);                                  //set LCD backlight on
 
  lcd.clear();                                             //clear LCD display
  lcd.setCursor(4,0);                                      //set LCD cursor to column 4, row 0
  lcd.print("BLUEsat");                                   //print BLUEsat to display with 5 leading spaces (you can change to your own)
  lcd.setCursor(1,1);                                      //set LCD cursor to column 0, row 1 (start of second line)
  lcd.print("Satellite Power");
  lcd.setCursor(1,2);
  lcd.print("DC Electronic Load"); //
  lcd.setCursor(1,3);
  lcd.print("Version 1.0"); //
  delay(3000);                                             //3000 mSec delay for intro display
  lcd.clear();                                             //clear dislay

}

// prints at start if all I2C devices can be found
void LCDI2CCheckGood(){

  lcd.begin(20, 4);                                        //set up the LCD's number of columns and rows 
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("I2C devices good");
  lcd.setCursor(0,1);
  lcd.print("Disconnect loads");
  lcd.setCursor(0,2);
  lcd.print("Zeroing inputs");
  delay(2000);
  lcd.clear();
  
}

// prints at start if not all I2C devices can be found
void LCDI2CCheckBad(){

  lcd.begin(20, 4);                                        //set up the LCD's number of columns and rows 
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("I2C issues");
  lcd.setCursor(0,1);
  lcd.print("Check cross power");
  lcd.setCursor(0,2);
  lcd.print("Debug w/ serial");
  delay(2000);
  lcd.clear();
    
}

//----------------------Display Rotary Encoder Input Reading on LCD---------------------------
void displayEncoderReading (void) {

    lcd.setCursor(8,2);                                      //start position of setting entry

    if ( ( Mode == "CP" || Mode == "CR" ) && reading < 100 ) {
        lcd.print("0");
    }
    
    if (reading < 10) {                                      //add a leading zero to display if reading less than 10
        lcd.print("0"); 
    }

    if ( Mode == "CP" || Mode == "CR" ) {
        lcd.print (reading, 2);                              //show input reading from Rotary Encoder on LCD
    } else {
        lcd.print (reading, 3);
    }
    //lcd.setCursor (CP, 2);                                   //sets cursor position
    //lcd.cursor();                                            //show cursor on LCD
}

