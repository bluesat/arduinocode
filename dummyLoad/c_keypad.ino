// reads from keypad input
void readKeypadInput (void) {
  customKey = customKeypad.getKey();
  
 // if (customKey != NO_KEY){                             //only used for testing keypad
 //Serial.print("customKey = ");                          //only used for testing keypad
 //Serial.println(customKey);                             //only used for testing keypad
 // }                                                     //only used for testing keypad
/*
  if(customKey == '#'){                                      //check if Zero Offset Selected (press * and Cursor Button together){
    toggle = false;                                         //switch Load OFF
    zeroOffset();
  }
  
  if(customKey == '*'){                                     //check if Set-Up Mode Selected (press * and Cursor Button together)
    delay(200);
    toggle = false;                                         //switch Load OFF
    userSetUp();
    index = 0;
    z = 0;
    decimalPoint = (' ');                                   //clear decimal point text character reset
  }
          */       
  if(customKey == 'A'){                                   //check if Constant Current button pressed
    toggle = false;                                         //switch Load OFF
    lcd.setCursor(8,0);
    lcd.print("OFF");
    Current();                                              //if selected go to Constant Current Selected routine
    index = 0;
    z = 0;
    decimalPoint = (' ');                                   //clear decimal point test character reset
    }
           
  if(customKey == 'B'){                                   //check if Constant Power button pressed
    toggle = false;                                         //switch Load OFF
    lcd.setCursor(8,0);
    lcd.print("OFF"); 
    Power();                                                //if selected go to Constant Power Selected routine
    index = 0;
    z = 0;
    decimalPoint = (' ');                                   //clear decimal point test character reset
  }
            
  if(customKey == 'C'){                                   //check if Constant Resistance button pressed  
    toggle = false;                                         //switch Load OFF
    lcd.setCursor(8,0);
    lcd.print("OFF");  
    Resistance();                                           //if selected go to Constant Resistance Selected routine
    index = 0;
    z = 0;
    decimalPoint = (' ');                                   //clear decimal point test character reset
  }

  /*
    if(customKey == 'D'){                                   //check if Battery Capacity button pressed
    dac.setVoltage(0,false);                                //Ensures Load is OFF - sets DAC output voltage to 0
    toggle = false;                                         //switch Load OFF
    batteryType();                                          //select battery type
    index = 0;
    z = 0;
    decimalPoint = (' ');                                   //clear decimal point test character reset
  
      if (exitMode == 1){                                   //if NO battery type selected revert to CC Mode
      lcd.setCursor(8,0);
      lcd.print("OFF");
      Current();                                            //if selected go to Constant Current Selected routine
      encoderPosition = 0;                                  //reset encoder reading to zero
      customKey = 'A';
      }
      else
      {
      lcd.setCursor(16,2);
      lcd.print(BatteryType);                               //print battery type on LCD 
      lcd.setCursor(8,0);
      lcd.print("OFF");
      timer.reset();                                        //reset timer
      BatteryLifePrevious = 0;
      BatteryCapacity();                                    //go to Battery Capacity Routine
      }
    }
    */
  
  if (Mode != "BC"){
  
    if(customKey >= '0' && customKey <= '9'){               //check for keypad number input
         numbers[index++] = customKey;
         numbers[index] = '\0';
         lcd.setCursor(z,3);                              
         lcd.print(customKey);                              //show number input on LCD
         z = z+1;
       }
    
    if(customKey == '*'){                                   //check if decimal button key pressed
        if (decimalPoint != ('*')){                         //test if decimal point entered twice - if so skip 
        numbers[index++] = '.';
        numbers[index] = '\0';
        lcd.setCursor(z,3);
        lcd.print(".");
        z = z+1;
        decimalPoint = ('*');                             //used to indicate decimal point has been input
          }
        }
  
    if(customKey == '#') {                                //check if Load ON/OFF button pressed
          x = atof(numbers);     
           reading = x;
           // encoderPosition = reading*1000;
           index = 0;
           numbers[index] = '\0';
           z = 0;
           lcd.setCursor(0,3);
           lcd.print("        ");
           decimalPoint = (' ');                          //clear decimal point test character reset
            }
      }
    
}

//------------------------Key input used for Battery Cut-Off and Transient Mode------------------------
void inputValue (void){

 while(customKey != '#'){
  
  customKey = customKeypad.getKey();
  if(customKey >= '0' && customKey <= '9'){               //check for keypad number input
       numbers[index++] = customKey;
       numbers[index] = '\0';
       lcd.setCursor(z,r);                              
       lcd.print(customKey);                              //show number input on LCD
       z = z+1;
     }
  
  if(customKey == '*'){                                   //check if ZERO READING key pressed
      if (decimalPoint != ('*')){                         //test if decimal point entered twice - if so ski
      numbers[index++] = '.';
      numbers[index] = '\0';
      lcd.setCursor(z,r);
      lcd.print(".");
      z = z+1;
      decimalPoint = ('*');                               //used to indicate decimal point has been input
        }
      }

 if(customKey == 'C'){                                    //clear entry
    index = 0;
    z = y;
    lcd.setCursor(y,r);
    lcd.print("     ");
    numbers[index] = '\0';                                //
    decimalPoint = (' ');                                 //clear decimal point test character reset
  }

 }
 
  if(customKey == '#') {                                  //check if Load ON/OFF button pressed
    x = atof(numbers);     
    index = 0;
    numbers[index] = '\0';
    decimalPoint = (' ');                                 //clear decimal point test character reset
  }
}

// allows the user to setup their system
void userSetUp (void) {
  y = 14;
  z = 14;
    
  lcd.noCursor();                                       //switch Cursor OFF for this menu               
  lcd.clear();
  lcd.setCursor(4,0);
  lcd.print("User Set-Up");
  lcd.setCursor(0,1);
  lcd.print("Current Limit=");
  lcd.setCursor(19,1);
  lcd.print("A");
  r = 1;
  inputValue();
  CurrentCutOff = x;
  lcd.setCursor(14,r);
  lcd.print(CurrentCutOff,3);
  
  customKey = '0';

  z = 14;

  lcd.setCursor(0,2);
  lcd.print("Power Limit  =");
  lcd.setCursor(19,2);
  lcd.print("W");
  r = 2;
  inputValue();
  PowerCutOff = x;
  lcd.setCursor(14,r);
  lcd.print(PowerCutOff,2);
  
  customKey = '0';
  
  z = 14;
  
  lcd.setCursor(0,3);
  lcd.print("Temperature  =");
  lcd.setCursor(18,3);
  lcd.print((char)0xDF);
  lcd.print("C");
  r = 3;
  inputValue();
  tempCutOff = x;
  lcd.setCursor(14,r);
  lcd.print(tempCutOff);

  //delay(500);                                             //used in testing only
  lcd.clear();

  lcd.setCursor(8,0);
  lcd.print("OFF");
  Current();                                            //if selected go to Constant Current Selected routine
  customKey = 'A';
  
}

