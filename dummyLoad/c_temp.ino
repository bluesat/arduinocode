void temperatureCutOff (void){

  getTemp(); // retrieve the temperature
  
  if (temp >= tempCutOff){                                 //if Maximum temperature is exceeded
    reading = 0;
    lcd.setCursor(0,3);
    lcd.print("                    ");
    lcd.setCursor(0,3);
    lcd.print("Over Temperature");
    lcd.setCursor(8,0);
    lcd.print("OFF");
    toggle = false;                                         //switch Load Off
  }
  
}

// pulls the temperature from the thermal setting
void getTemp(){

  Wire.beginTransmission(I2C_ADD_THERMAL);
  Wire.write(byte(0x00));
  Wire.endTransmission();
  
  Wire.requestFrom(I2C_ADD_THERMAL, 1);
  if (1 <= Wire.available()) { // if two bytes were received
    temp = Wire.read();  // receive high byte (overwrites previous reading)
  }

}


//-----------------------Fan Control----------------------------------------------------------
void fanControl (void) {

  getTemp(); // retrieve the temperature

  //Serial.print("Temp = ");                          //used for testing only
  //Serial.println(temp);                             //used for testing only

  if (temp < TEMP_FAN_MIN-4) {                                    //is temperature lower than really cold always turn off fan
      fanSpeed = 0;
      digitalWrite(PIN_FAN, LOW);                          //then fan turned off
  }

  if (temp >= TEMP_FAN_MIN+4) {      //Hysteresis to avoid fan starting and stopping around set point - needs to be large hystersis as fan causes thermal sensor to change temp easily
      fanSpeed = 131;
      digitalWrite(3, HIGH);
      //analogWrite(PIN_FAN, fanSpeed);
  }

/*
  if (temp >= 32  && temp < 40 ) {                     //Below 40 we run fan fixed at minimum
      fanSpeed = 131;
      analogWrite(PIN_FAN, fanSpeed);
  }
  

  if ((temp >= 40) && (temp < 50)){                    //OK we need the fan but let us keep it quiet if we can
      fanSpeed = map(temp, 40, 50, 131, 200);
      analogWrite(PIN_FAN, fanSpeed);
 }

  if ((temp >= 50) && (temp <= tempMax)){
      fanSpeed = map(temp, 50, 75, 131, 255);           //OK we need a jet stream and hearing protection
      analogWrite(PIN_FAN, fanSpeed);
  }

  if (temp > tempMax) {                                     //OK we need fan at full steam. Ready for take off
      fanSpeed = 255;
      digitalWrite(PIN_FAN, HIGH);
  }
*/
  lcd.setCursor(16,0);
  lcd.print(temp);
  lcd.print((char)0xDF);
  lcd.print("C");

  //Serial.print("Fan Speed ");                      //used for testing only
  //Serial.println(fanSpeed);                        //used for testing only
  
}
