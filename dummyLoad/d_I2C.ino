int checkAddress(byte address){

    byte error = 0;
    int retVal = -1;

    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
 
    if (error == 0){
      Serial.print("I2C device found at address 0x");
      retVal = 1;
    }
    else if (error==4){
      Serial.print("Unknown error at address 0x");
      retVal = 0;
    }    
    else{
      Serial.print("I2C device NOT found at address 0x");
      retVal = 0;
    }

  if (address<16){
    Serial.println("0");
  }
  Serial.println(address,HEX);

  return retVal;

}

void i2c_scanner(){

  byte error, address;
  int nDevices;
 
  Serial.println("Scanning...");
 
  nDevices = 0;
  for(address = 1; address < 127; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
 
    if (error == 0)
    {
      Serial.print("I2C device found at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.print(address,HEX);
      Serial.println("  !");
 
      nDevices++;
    }
    else if (error==4)
    {
      Serial.print("Unknow error at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.println(address,HEX);
    }    
  }
  
  if (nDevices == 0)
    Serial.println("No I2C devices found\n");
  else
    Serial.println("done\n");

}

// checks if all the specified devices are present on the line
int checkI2CDevices(){

  if (checkAddress(I2C_ADD_DAC) & checkAddress(I2C_ADD_ADC) & checkAddress(I2C_ADD_RTC) & checkAddress(I2C_ADD_RTC) & checkAddress(I2C_ADD_THERMAL)){
    LCDI2CCheckGood();
    return 1;
  }
  else{
    LCDI2CCheckBad();
    return 0;
  }

}

