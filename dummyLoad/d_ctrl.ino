//-----------------------Select Constant Current LCD set up--------------------------------
void Current(void) {
  
  Mode = ("CC");
  lcd.setCursor(0,0);
  lcd.print("DC LOAD");  
  lcd.setCursor(0,2);
  lcd.print("                ");
  lcd.setCursor(0,2);
  lcd.print("Set I = ");
  lcd.setCursor(16,2);
  lcd.print("    ");
  lcd.setCursor(14,2);
  lcd.print("A");
  lcd.setCursor(0,3);                                   //clear last line of time info
  lcd.print("                    ");                    //20 spaces so as to allow for Load ON/OFF to still show
  // CP = 9;                                               //sets cursor starting position to units.
  
}

//----------------------Select Constant Power LCD set up------------------------------------
void Power(void) {
  
  Mode = ("CP");
  lcd.setCursor(0,0);
  lcd.print("DC LOAD");
  lcd.setCursor(0,2);
  lcd.print("                ");
  lcd.setCursor(0,2);
  lcd.print("Set W = ");
  lcd.setCursor(16,2);
  lcd.print("    ");
  lcd.setCursor(14,2);
  lcd.print("W");
  lcd.setCursor(0,3);                                   //clear last line of time info
  lcd.print("                    ");                    //20 spaces so as to allow for Load ON/OFF to still show
  // CP = 10;                                               //sets cursor starting position to units.
  
}

//----------------------- Select Constant Resistance LCD set up---------------------------------------
void Resistance(void) {
  
  Mode = ("CR");
  lcd.setCursor(0,0);
  lcd.print("DC LOAD");  
  lcd.setCursor(0,2);
  lcd.print("                ");
  lcd.setCursor(0,2);
  lcd.print("Set R = ");
  lcd.setCursor(16,2);
  lcd.print("    ");
  lcd.setCursor(14,2);
  lcd.print((char)0xF4);
  lcd.setCursor(0,3);                                   //clear last line of time info
  lcd.print("                    ");                    //20 spaces so as to allow for Load ON/OFF to still show
  // CP = 10;                                               //sets cursor starting position to units.
  
}



//----------------------Power Level Cutoff Routine-------------------------------------------
void powerLevelCutOff (void) {
  
  if (ActualPower  > PowerCutOff){                        //Check if Power Limit has been exceed
    reading = 0;
    // encoderPosition = 0; 
    lcd.setCursor(0,3);
    lcd.print("                    ");
    lcd.setCursor(0,3);
    lcd.print("Exceeded Power");
    lcd.setCursor(8,0);
    lcd.print("OFF");
    toggle = false;                                         //switch Load Off
  }
  
}

//----------------------Limit Maximum Current Setting-----------------------------------------
void maxConstantCurrentSetting (void) {
  
  if (Mode == "CC" && reading > CurrentCutOff){           //Limit maximum Current Setting
    reading = CurrentCutOff;
    lcd.setCursor(0,3);
    lcd.print("                    ");                      //20 spaces to clear last line of LCD 
  }

  if (Mode == "CP" && reading > PowerCutOff) {             //Limit maximum Current Setting
    reading = PowerCutOff;
    lcd.setCursor(0,3);
    lcd.print("                    ");                   //20 spaces to clear last line of LCD 
  }

   if (Mode == "CR" && reading > ResistorCutOff ) {             //Limit maximum Current Setting
    reading = ResistorCutOff;
    lcd.setCursor(0,3);
    lcd.print("                    ");                   //20 spaces to clear last line of LCD 
  }

}

//-----------------------Toggle Current Load ON or OFF------------------------------
void LoadSwitch(void) {

   delay(100);                                          //simple delay for key debounce (commented out if not required)
  
  if (digitalRead(PIN_LOAD_ON_OFF) == LOW) {
    lcd.setCursor(8,0);
    lcd.print("On  ");
    lcd.setCursor(0,3);
    lcd.print("                    ");                 //clear bottom line of LCD
    //Load = 0;
    toggle = true;        
  }
  else{
    lcd.setCursor(8,0);
    lcd.print("Off ");
    //Load = 1;
    toggle = false;
  }
  
}
