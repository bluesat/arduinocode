void testDAC();
void testRTC();

// outputs voltages that increase
// THIS WILL DRIVE THE MOSFET IF USED IN THE COMPLETE CIRCUIT - UNSAFE AS COULD DRAW LARGE AMOUNTS OF CURRENT
void testDAC(){
  
    Serial.println("Start: Testing DAC...");
    
    Serial.println("Output: 0V");
    dac.setVoltage(DAC_BASIS*0/DAC_VOLTAGE, false);
    delay(DELAY_DAC);
    
    Serial.println("Output: 1V");
    dac.setVoltage(DAC_BASIS*1/DAC_VOLTAGE, false);
    delay(DELAY_DAC);

    Serial.println("Output: 2V");
    dac.setVoltage(DAC_BASIS*2/DAC_VOLTAGE, false);
    delay(DELAY_DAC);

    Serial.println("Output: 3V");
    dac.setVoltage(DAC_BASIS*3/DAC_VOLTAGE, false);
    delay(DELAY_DAC);

    Serial.println("Output: 4V");
    dac.setVoltage(DAC_BASIS*4/DAC_VOLTAGE, false);
    delay(DELAY_DAC);

    Serial.println("Finish: Testing DAC\n");
}

// measures time after various intervals to check RTC measurements
void testRTC(){

   Serial.println("Start: Testing RTC...");

    uint32_t seconds = 0;
    String strTime;

    // reset the timer
    timer.reset();

    // start the timer
    timer.start();

    delay(50); // to avoid rounding errors from operating near a second exactly

    for(int i = 1; i <= 5; i++){
      delay(i*DELAY_RTC);
      seconds = timer.getTotalSeconds();
      strTime = timer.getTime();
      // print on the console
      // Serial.println(seconds);
      Serial.println(strTime);
    }

    // stop the timer
    timer.stop();

    // reset the timer
    timer.reset();

    Serial.println("Finish: Testing RTC\n");

}

// reads the channel one input to make sure the ADC is measuring reasonably
void testADC(){

   Serial.println("Start: Testing ADC...");

  long value = 0;
  MCP342x::Config status;
  
  // Initiate a conversion; convertAndRead() will wait until it can be read
  adc.convertAndRead(MCP342x::channel1, MCP342x::oneShot,
           MCP342x::resolution16, MCP342x::gain1,                         //"gain1" means we have select the input amp of the ADC to x1
           1000000, value, status);
  
  Serial.print("ADC Channel 1: ");
  Serial.print(((value*2.048)/32767) * 2.5);
  Serial.println("V");

  lcd.print("ADC Channel 1: ");
  lcd.print(((value*2.048)/32767) * 2.5);
  lcd.print("V");

  lcd.print("Rail Voltage: ");
  lcd.print(((value*2.048)/32767) * 2.5 * 50);
  lcd.print("V");

  Serial.println("Finish: Testing ADC\n");

  
}

// allows divide by 10 resistor to be precisely set
void DACcalibrate(){

  Serial.println("Calibrating DAC...");
  
  dac.setVoltage(DAC_BASIS*4/DAC_VOLTAGE, false);
  Serial.println("Output voltage at 4V, adjust until 1/10 divider accurate");
  delay(10000);
  
  dac.setVoltage(DAC_BASIS*2/DAC_VOLTAGE, false);
  Serial.println("Output voltage at 2V, check 1/10 divider is still accurate");
  delay(50000);

  Serial.println("DAC calibrated...\n");

  
}

// pulls the temperature from the thermal setting
void testThermal(){

  Serial.println("Start: Testing thermal...");


  int8_t reading = 0;

  Wire.beginTransmission(I2C_ADD_THERMAL);
  Wire.write(byte(0x00));
  Wire.endTransmission();
  
  Wire.requestFrom(I2C_ADD_THERMAL, 1);
  if (1 <= Wire.available()) { // if two bytes were received
    reading = Wire.read();  // receive high byte (overwrites previous reading)
  }

  Serial.print("Temperature is: ");
  Serial.print(reading);   // print the reading
  Serial.println("C");

  Serial.println("Finish: Testing thermal\n");

}

void printCustom(char text[]){

  Serial.println(text);

  if(TEST_LCD){
        lcd.print(text);
  }
  
}

// changes the DAC default output to 0V (by writing to its EEPROM)
// all values are currently hardcoded
// from datasheet C2 = 0, C1 = 1, C0 = 1 to write to EEPROM
void changeDACDefault(){

  Wire.beginTransmission(I2C_ADD_DAC);
  Wire.write(0b01100000); // C2, C1, C0, XX, PD1, PD0, X
  Wire.write(0x00); // D11 - D4
  Wire.write(0x00); // D3 - D0, XXXX
  Wire.endTransmission();
  
}

