void setup() {

  Serial.begin(9600);                                      //used for testing only

  Wire.begin();                                            //join i2c bus (address optional for master)
  Wire.setClock(400000L);                                  //sets bit rate to 400KHz

  // need to reset DAC straight away as default EEPROM setting is driving the DAC at 1/2 (should be fixed now by changing it EEPROM default)
  dac.begin(I2C_ADD_DAC);
  dac.setVoltage(0,false);                                 //reset DAC to zero for no output current set at Switch On

  if(TEST_I2C){
    i2c_scanner();
  }

  checkI2CDevices();
  delay(1000);

  pinMode(PIN_FAN, OUTPUT);
  TCCR2B = (TCCR2B & 0b11111000) | 1;                      //change PWM to above hearing
  pinMode (PIN_LOAD_ON_OFF, INPUT_PULLUP);


  MCP342x::generalCallReset();
  delay(1); // MC342x needs 300us to settle, wait 1ms

  LCDStart();

  lcd.setCursor(8,0);
  lcd.print("OFF");                                        //indicate that LOAD is off at start up
  Current();                                               //sets initial mode to be CC (Constant Current) at Power Up

  customKey = customKeypad.getKey();

  // zeroOffset();
  
}
