void loop() {

  readKeypadInput();                                     //read Keypad entry
  LoadSwitch();                                          //Load on/off

  lcd.setCursor(18,3);                                   //sets display of Mode indicator at bottom right of LCD
  lcd.print(Mode);                                       //display mode selected on LCD (CC, CP, CR or BC)
  
  temperatureCutOff();                                   //check if Maximum Temperature is exceeded

  if(Mode != "TC" && Mode != "TP" && Mode != "TT"){      //if NOT transient mode then Normal Operation
    // reading = encoderPosition/1000;                        //read input from rotary encoder 
    maxConstantCurrentSetting();                           //set maxiumum Current allowed in Constant Current Mode (CC)
    powerLevelCutOff();                                    //Check if Power Limit has been exceeded
    displayEncoderReading();                               //display rotary encoder input reading on LCD
  }

  readVoltageCurrent();                                  //routine for ADC's to read actual Voltage and Current
  ActualReading();                                       //Display actual Voltage, Current readings and Actual Wattage
  
  dacControl();
  dacControlVoltage();                                   //sets the drive voltage to control the MOSFET

  fanControl();                                          //call heatsink fan control

}
