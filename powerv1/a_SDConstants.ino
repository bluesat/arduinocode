//*********************************************************************************
// LIBRARIES
//*********************************************************************************

// IMPORTANT: The default SD card library defines SCL_PIN and SDA_PIN, it does not use these variables again and these conflict with the I2C library
// SD library file SD2PinMap.h must be edited to comment out all definition of SCL_PIN and SDA_PIN

#include <SPI.h>
#include <SD.h>

//*********************************************************************************
// SD CONSTANTS
//*********************************************************************************

#define CS_PIN 10 // Arduino pin connected to CS of SD card

//*********************************************************************************
// GLOBAL VARIABLES
//*********************************************************************************

Sd2Card card;
SdVolume volume;
SdFile root;

// File dataFile; // global variable for file

#define FILENAME "DATALOG.TXT" // file name to write to

#define WRITE_STATUS WRITE
#define WRITE 1
#define NO_WRITE 0
