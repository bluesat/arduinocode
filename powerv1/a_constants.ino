// Define constants here
// Include additional libraries here as well
// Global variables are currently here

//*********************************************************************************
// PROGRAMMING PARAMETERS
//*********************************************************************************

// constant that is used to print additional debugging information (1 for print, 0 for normal - otherwise poorly defined) 
#define DEBUG_VERBOSE 1

// used to enable/disable sleep mode on devices
#define SLEEP_MODE_OFF 0
#define SLEEP_MODE_ON 1

#define KILO 1000 // converts over 1000x

//*********************************************************************************
// CHIP ADDRESSES
//*********************************************************************************

// addresses on the system (all are 7 bits with the last bit a zero to make it read/write)
#define I2C_ADC_ADDR_1 B00010000 // analog to digital converter  - see http://cds.linear.com/docs/en/datasheet/23015fb.pdf
#define I2C_IO_ADDR_1 B01000000 // input output expander - has different base addresses TCA9554A/NOT TCA9554 - see http://www.ti.com/product/TCA9554A
#define I2C_POT_ADDR_1 B10100000 // potentiometer - see http://www.intersil.com/content/dam/Intersil/documents/isl2/isl22326.pdf

#define I2C_ADC_ADDR_2 B00110110 // analog to digital converter
#define I2C_IO_ADDR_2 B01000010 // input output expander
#define I2C_POT_ADDR_2 B10100100 // potentiometer

#define I2C_ADC_ADDR_GLOBAL 0b1101011 // global address for all ADC chips to allow for synchronisation conversion

// 7 bit I2C addresses are reported by the i2c_scanner program (http://playground.arduino.cc/Main/I2cScanner)
//I2C device found at address 0x08 = 0b0001000 ! = ADC_ADDR_1
//I2C device found at address 0x1B = 0b0011011 ! = ADC_ADDR_2 (? appears to be set for AD1 = HIGH and AD0 = float)
//I2C device found at address 0x20 = 0b0100000 ! = IO_ADDR_1
//I2C device found at address 0x21 = 0b0100001 ! = IO_ADDR_2
//I2C device found at address 0x50 = 0b1010000 ! = POT_ADDR_1
//I2C device found at address 0x52 = 0b1010010 ! = POT_ADDR_2
//I2C device found at address 0x6B = 0b1101011 ! = ADC_ADDR_GLOBAL

//*********************************************************************************
// I2C/DELAYS
//*********************************************************************************

// pinout defined for I2C library (ARDUINO DEPENDENT)
// see http://3.bp.blogspot.com/-5bIrGV8-TfE/VKSNL21TULI/AAAAAAAAAAk/UC4vz6oc-Wg/s1600/ARDUINO.Mega.Pinout.Diagram.png for Arduino Mega reference (PD0 = SCL on 21 and PD1 = SDA on 20) - more options exist for Mega if needed)
// see https://camo.githubusercontent.com/0174ab1436903eb06dfea280a83c389460637054/68747470733a2f2f7261776769746875622e636f6d2f426f756e692f41726475696e6f2d50696e6f75742f6d61737465722f41726475696e6f253230556e6f253230523325323050696e6f75742e706e67 for Arduino Uno (PC4 = SDA on A4 and PC5 = SCL on A5)
// see http://www.pighixxx.com/test/wp-content/uploads/2014/11/nano.png for nano (PC5 = SCL on AC5 and PC4 = SDA on AC4)

// currently set for Arduino Uno
#define SCL_PIN 5
#define SDA_PIN 4
#define SCL_PORT PORTC
#define SDA_PORT PORTC

// for the i2c_read function, if argument is true then NACK bit is sent (indicates end of transfer sequence) 
#define NACK 1
#define ACK 0

// I2C read/write bits (last bit of I2C address)
#define WRITE 0
#define READ 1

// various delays
#define DELAY_I2C 10 // generic delay time to account for lag in milliseconds
#define DELAY_PULSE 10 // time to pulse the reset pin on the latching comparator to reset (in microseconds)
#define DELAY_END 5120 // time to update most registers (some are 5.12 seconds) in milliseconds
#define DELAY_TEST 5000 // time to wait test measurements at stable environment
#define DELAY_STABLE 500 // time to allow circuit to settle after changes made to stabilise it
#define DELAY_PRINT 1000 // gives time to print text and allow Arduino to be ready for next time sensitive command
#define DELAY_WAKE_ADC 200 // time for ADC to fully charge capacitors and fully wake in milliseconds
#define DELAY_MEASURE_ADC 5 // time to allow ADC to acquire information (in microseconds) (datasheet says 250ns is sufficient - this may be excessive)
#define DELAY_LED 500 // time to hold an LED for a status indicator
#define DELAY_CAP 7500 // time to allow the capacitors to charge before turning on

// default function exit statuses
#define EXIT_COMPLETE 1
#define EXIT_ERROR -2

// Arduino IO pin attached to driving of relay
#define RELAY_PIN 5

//*********************************************************************************
// CIRCUIT PARAMETERS
//*********************************************************************************

#define NUM_WIPER_TAPS 127 // number of taps in the potentiometer
#define RES_POT 10000 // 10kOhm potentiometer (was 50kOhm in old design)
#define RES_SENSE 0.05 // 50 milliOhm sense resistor
#define RES_IN 200 // 200 Ohm resistor into LT6108
#define VOLTAGE_REFERENCE 0.4 // 400mV reference voltage for comparator to trip

// 7 bit resistance code (0-127 are valid)
// 128 resistor taps for the ISL22326 are available, providing this precision limit
#define RMAX 127
#define RMIN 0

// IO expander pins
#define PIN_5_EN 6
#define PIN_5_RESET 5
#define PIN_3_EN 1
#define PIN_3_RESET 2

#define CURRENT_LIMIT_DEFAULT 10.0 // default current limit to apply to latched rails during setup (should be a float)
#define CURRENT_LIMIT_TEST_LOW 0.035 // reduced current limit to force an overcurrent event during testing

//*********************************************************************************
// MEMORY LOCATIONS
//*********************************************************************************

// memory locations on the ADC
// http://cds.linear.com/docs/en/datasheet/23015fb.pdf
#define ADC5 B11001000 // config byte for single ended bit on CH1
#define ADC3 B10001000 // config byte for single ended bit on CH0

// memory locations on the I2C potentiometer for the wiper position (WRi)
// https://www.intersil.com/content/dam/Intersil/documents/isl2/isl22326.pdf
#define MEM_POT_ACR 8 // ACR memory location
#define MEM_POT_3 B00000000 // 5V pot memory location at pot 1
#define MEM_POT_5 B00000001 // 3V pot memory location at pot 0
#define POT_ACR B1100000 // ACR = access control register
// VOL (1 = volatile/0 = both) + SHDN (1 = normal/0 = shutdown) + WIP (read only bit indicating non-volatile write in operation)

// memory locations on the IO expander
// http://www.ti.com/lit/ds/symlink/tca9554.pdf
#define IO_MEM B00000011 // register 3 is the configuration register
#define IO_OUTPUT B00000001 // register 1 is the output pin register
#define IO_INVERT B00000010 // register 2 is the invert polarity register

#define IO_CHANNEL_SETTING B00000000 // sets all channels to outputs
#define IO_CONFIG_REG B1001001 // 0 = output and 1 = input, pins are in order of [76543210]
// P1 = 3 Manual, P2 = 3 Reset, P5 = 5 Reset, P6 = 5 Manual

//*********************************************************************************
// ADC CONSTANTS
//*********************************************************************************

// maximum (12 bit resolution 2^12 = 4096)
#define ADC_MAX 4095

// power rail for ADC (used to convert ADC value into voltage)
#define REFCOMP_ADC 4096

#define CHANNEL_3V3 0 // 3V3 current measurement input into ADC chip
#define CHANNEL_5V 1 // 5V current measurement input into ADC chip

// config bits for 6-bit Din word to program modes of operation (sent as 8 bits)
// [S/D, O/S, X, X, UNI/BI, SLP, X, X] - X is ignored
// measured from first rising edge, currently a byte but may need to force a 6-bit word
#define ADC_SINGLE_CONFIG B10000000 // config bit for single-ended measurement
#define ADC_DIFF_CONFIG B00000000 // config bit for differential measurement
#define ADC_CH0_CONFIG B01000000 // config bit for measuring relative to channel 0 as positive (also ODD)
#define ADC_CH1_CONFIG B00000000 // config bit for measuring relative to channel 1 as positive (also SIGN)
#define ADC_UNIPOLAR_CONFIG B00001000 // config bit for unipolar measurements
#define ADC_BIPOLAR_CONFIG B00000000 // config bit for bipolar measurements
#define ADC_SLEEP_MODE_ON B00000100 // config bit for enabling sleep mode
#define ADC_SLEEP_MODE_OFF B00000000 // set the last bit to 1 to force a sleep mode

//*********************************************************************************
// POTENTIOMETER CONSTANTS
//*********************************************************************************

// 3 config bits for the access control register (ACR)
#define POT_NVOL_ACCESS B00000000 // allows access to non-volatile registers only
#define POT_NVOL_BLOCK B10000000 // only volatile registers can be accessed
#define POT_SHDN_ON B00000000 // shutdown active
#define POT_SHDN_OFF B01000000 // shutdown inactive

#define MAX_RES_BYTE 127.00 // maximum value of resistance byte obtained from pot

//*********************************************************************************
// GLOBAL VARIABLES (LOOK INTO MAKING THESE DIFFERENT)
//*********************************************************************************

// position of each IO configuration bit in the IO output array
#define IO_POS_5_EN 0
#define IO_POS_5_RESET 1
#define IO_POS_3_EN 2
#define IO_POS_3_RESET 3

// ADC pins on the Arduino to measure the rail voltages
#define PIN_ARD_5V 7
#define PIN_ARD_3V3 6

#define VOLTAGE_ARD 5.0 // arduino reference voltage

boolean LED_ARDUINO_STATUS = false;  // toggle for LED on the Arduino

// IO pin settings for startup
// one set is needed for each IO expander
// global variables are needed to make these persistent (could look into passing them as an array instead)
boolean IO_5_EN_1 = true;
boolean IO_5_RESET_1 = true;
boolean IO_3_EN_1 = true;
boolean IO_3_RESET_1 = true;

// array version of above method (not currently used)
// boolean IO_1[4] = {true, true, true, true};
// boolean IO_2[4] = {true, true, true, true};

boolean IO_5_EN_2 = true;
boolean IO_5_RESET_2 = true;
boolean IO_3_EN_2 = true;
boolean IO_3_RESET_2 = true;

long CYCLE_COUNTER = 0;

//*********************************************************************************
// LIBRARIES
//*********************************************************************************

#include <SoftI2CMaster.h>
