// define ADC low level functions here

//*********************************************************************************
// FUNCTION DECLARATION
//*********************************************************************************

int readCurrent(byte addr, int channel); // read current from ADC
double convertCurrent(int datai); // convert current from integer to readable double
int setupADC(byte addr, int channel, int sleepMode); // sets up the ADC with a 6-bit word

//*********************************************************************************
// FUNCTIONS
//*********************************************************************************

// PURPOSE: measure the current from the ADC
// ARGUMENTS: addr = 8 bit address of chip, channel (0/1 are valid channels to measure)
// RETURN VALUES: data (integer containing voltage information)
// send a STOP signal to the ADC to initiate conversion
// wait 250ns and device is ready for a read/write request 
// read two bytes
// data is in straight binary for unipolar mode (used) and 2's complement for bipolar
int readCurrent(byte addr, int channel) {

  uint16_t data;
  byte MSB;
  byte LSB;

 // ensures the ADC is correctly setup for measuring the data
  if(setupADC(addr, channel, SLEEP_MODE_OFF)!=EXIT_COMPLETE){
    Serial.println(F("Error in setting up ADC for measurement"));
    return EXIT_ERROR;
  }

  i2c_start(addr|I2C_WRITE); // sends identification byte
  
  i2c_stop(); // send STOP condition to start measurement
  delayMicroseconds(DELAY_MEASURE_ADC); // pause to allow measurement to complete (250ns should be sufficient)
  
  i2c_start(addr|I2C_READ); // sends identification byte to start reading

  // receive 2 8 bit numbers from the program
  MSB = i2c_read(ACK);
  LSB = i2c_read(NACK); // padded with zeros for last 4 bits

  if(DEBUG_VERBOSE){
    // printing the received bits for debugging
    Serial.print("MSB: ");
    Serial.println(MSB, BIN);
    Serial.print("LSB: ");
    Serial.println(LSB, BIN);
  }

  i2c_stop(); // send STOP condition to indicate end of measurement

  // combine the 2 bytes into a 16 bit integer
  data = ((MSB << 8)|LSB) >> 4; // shift 4 bits to right as data is only 12 bits long (pad with front zeros, not rear)

  if(DEBUG_VERBOSE){
    // data obtained from ADC direct - for debugging
    Serial.print(F("Binary ADC input: "));
    Serial.println(data, BIN);
    Serial.print(F("Integer ADC input: "));
    Serial.println(data);
  }
  
  return data;

}

// PURPOSE: convert current from 12 bit number to a double
// ARUGMENTS: integer of current data
// RETURN VALUE: converted and shifted double of data
double convertCurrent(int datai) {

  double data = double(datai);

  double relativeADC;
  double voltageOut;
  double voltageIn;
  double current;

  if(DEBUG_VERBOSE){
    // ADC debugging
    Serial.print(F("Pure ADC output (in double): "));
    Serial.println(data, DEC);
  }

  // equations are ordered to ensure double type is maintained throughout
  relativeADC = data/ADC_MAX;
  voltageOut = relativeADC*REFCOMP_ADC; // voltage received at ADC
  voltageIn = (voltageOut*RES_IN)/RES_POT; // voltage received at CSA
  current = voltageIn/RES_SENSE; // voltage at CSA converted to current through sense resistor

  if(DEBUG_VERBOSE){
    Serial.print(F("Relative ADC out: "));
    Serial.println(relativeADC);
    Serial.print(F("Voltage out (i.e. recieved at the ADC) is: "));
    Serial.println(voltageOut);
    Serial.print(F("The voltage at the input to the CSA (converted through resistances) is: "));
    Serial.println(voltageIn);
    Serial.print(F("Current through the sense resistor: "));
    Serial.println(current);
  }

  return current;

}

// PURPOSE: sends a 6 bit word to the ADC to set it up correctly for measurement
// ARGUMENTS: addr = 8 bit address of chip, channel (0/1 are valid channels to measure), sleep mode (0/1 are off/on)
// RETURNS: exit status
int setupADC(byte addr, int channel, int sleepMode){

  byte channelConfig = 0; // stores the channel bit for the word
  byte sleepConfig = 0; // stores the sleep bit for the word
  byte configByte = 0; // stores the 6-bit word in a byte

  i2c_start(addr|I2C_WRITE); // sends identification byte

  // set the config bit to determine which channel to read from
  if (channel == CHANNEL_3V3) {
    channelConfig = ADC_CH0_CONFIG;
  }
  else if (channel == CHANNEL_5V) {
    channelConfig = ADC_CH1_CONFIG;
  }
  else {
    Serial.println(F("Error - Addressing invalid channel for the ADC - 0/1 are the only valid channels"));
    i2c_stop();
    return EXIT_ERROR;
  }

  // set the config bit to determine whether to sleep or not
  if (sleepMode == SLEEP_MODE_ON) {
    sleepConfig = ADC_SLEEP_MODE_ON;
  }
  else if (sleepMode == SLEEP_MODE_OFF) {
    sleepConfig = ADC_SLEEP_MODE_OFF;
  }
  else {
    Serial.println(F("Error - Invalid sleep command"));
    i2c_stop();
    return EXIT_ERROR;
  }

  configByte = ADC_SINGLE_CONFIG|channelConfig|ADC_UNIPOLAR_CONFIG|ADC_SLEEP_MODE_OFF;
  Serial.println(configByte, HEX);

  if(!i2c_write(configByte)) {
      Serial.println(F("Error - ADC didn't respond with ACK bit after setting config word"));
      i2c_stop();
      return EXIT_ERROR;
    }

  if(DEBUG_VERBOSE){
    Serial.println(F("ADC set up"));
    Serial.print(F("Config byte is: "));
    Serial.println(configByte, BIN);
  }

  i2c_stop();

  return EXIT_COMPLETE;
  
}
