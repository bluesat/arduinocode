// define IO expander low level functions here

//*********************************************************************************
// FUNCTION DECLARATION
//*********************************************************************************

int setIOChannels(byte addr); // initial set of the pin types on the IO expander
int setIOOutputs (byte addr, boolean IO_5_EN, boolean IO_5_RESET, boolean IO_3_EN, boolean IO_3_RESET); // set pins on the IO expander to control the lines
int resetIOCurrentSenseAmp(byte addr, int pin); // reset the current sense amp from a latched condition through controlling IO

//*********************************************************************************
// FUNCTIONS
//*********************************************************************************

// PURPOSE: set the pins on the IO expander as outputs
// ARGUMENTS: addr = 8 bit address of chip
// RETURN VALUE: returns an error integer/complete integer
int setIOChannels(byte addr) {

  i2c_start(addr|I2C_WRITE);

  if(!i2c_write(3)){
    Serial.println(F("ERROR - No ACK bit received from setting control register in IO expander to config"));
    i2c_stop(); // frees the bus line
    return EXIT_ERROR;
  }

  if(!i2c_write(IO_CHANNEL_SETTING)){
    Serial.println(F("ERROR - No ACK bit receieved from setting the pin configuration in IO expander"));
    i2c_stop(); // frees the bus line
    return EXIT_ERROR;
  }

  Serial.println(F("IO expander outputs set up"));

  i2c_stop(); // frees the bus line

  return EXIT_COMPLETE;
  
}

// PURPOSE: set the pins on the IO expander as high/low to control shutdown pins/reset pins
// ARGUMENTS: addr = 8 bit address of chip, 4x IO bits (stored as Booleans) that set pins as output/input
// RETURN VALUE: returns an error integer
int setIOOutputs (byte addr, boolean enable5V, boolean reset5V, boolean enable3V, boolean reset3V) {

  byte data = B00000000; // output pin settings to be written to IO expander (filled with boolean arguments)
  
  i2c_start(addr|I2C_WRITE);
  if(!i2c_write(IO_OUTPUT)) {
    Serial.println(F("ERROR - No ACK bit received from setting control register in IO expander to output"));
    i2c_stop(); // frees the bus line
    return EXIT_ERROR;
  }

  // 8 pins (0-7) each represented by a bit in the data byte sent to the IO expander
  // each bit location represents its corresponding pin (e.g. pin 5 is the 5th bit in the byte)
  // PIN_X_TYPE is the pin of the X volt rail controlling the TYPE
  if (enable5V == true) {
    data = data + (B00000001 << PIN_5_EN); // 1 is bitshifted over to the corresponding pin address if the boolean is true
  }
  if (enable3V == true) {
    data = data + (B00000001 << PIN_3_EN);
  }  
  if (reset5V == true) {
    data = data + (B00000001 << PIN_5_RESET);
  }
  if (reset3V == true) {
    data = data + (B00000001 << PIN_3_RESET);
  }

  if (DEBUG_VERBOSE) {
    Serial.print(F("CHECK - the output pins (from 7->0) are set as: "));
    Serial.println(data, BIN);
  }

  if(!i2c_write(data)) {
    Serial.println(F("ERROR - No ACK bit received from setting output pins in IO expander"));
    i2c_stop(); // frees the bus line
    return EXIT_ERROR;
  }

  Serial.println("IO expander output pin set");

  i2c_stop(); // free the bus again

  return EXIT_COMPLETE;

}


// PURPOSE: reset the current sense amplifier from a latched condition
// ARGUMENTS: addr is the address of the IO expander, pin is the pin to be reset
// RETURN VALUE: returns an error integer (1 indicates successful setting of data)
// Pull the EN/RST pin on the LT6108 high for the reset period of time
int resetIOCurrentSenseAmp(byte addr, int pin) {

  byte invertByteEmpty = B00000000;
  byte invertByte;

  i2c_start(addr|I2C_WRITE);

  if(!i2c_write(IO_INVERT)) {
    Serial.println(F("ERROR - No ACK bit received from setting control register in IO expander to invert"));
    i2c_stop(); // frees the bus line
    return EXIT_ERROR;
  }

  if (pin == PIN_5_RESET || pin == PIN_3_RESET) {
    invertByte = B00000001 << pin;
    Serial.println(invertByte, BIN);
  }
  else {
    Serial.print(F("Invalid pin selected: "));
    Serial.println(pin);
    i2c_stop(); // frees the bus line
    return EXIT_ERROR;  
  }

  if(!i2c_write(invertByte)) {
      Serial.println(F("ERROR - No ACK bit received from setting invert pins in IO expander"));
      i2c_stop(); // frees the bus line
      return EXIT_ERROR;
    }
    
  delayMicroseconds(DELAY_PULSE);

  if(!i2c_write(invertByteEmpty)) {
      Serial.println(F("ERROR - No ACK bit received from setting invert pins in IO expander"));
      i2c_stop(); // frees the bus line
      return EXIT_ERROR;
  }

  Serial.print(F("Reset pulse applied to pin: "));
  Serial.println(pin);

  i2c_stop(); // free the bus again

  return EXIT_COMPLETE;

}



