// other low level functions

//*********************************************************************************
// FUNCTION DECLARATION
//*********************************************************************************

int relayOpen(); // opens the relay
int relayClose(); // closes the relay
int LEDSwitch(); // toggles the Arduino LED

//*********************************************************************************
// FUNCTIONS
//*********************************************************************************

// PURPOSE: opens the relay
// ARGUMENTS: None
// RETURNS: EXIT_COMPLETE
int relayOpen() {

  digitalWrite(RELAY_PIN, HIGH);
  delay(DELAY_STABLE);

  return EXIT_COMPLETE;
  
}

// PURPOSE: closes the relay
// ARGUMENTS: None
// RETURNS: EXIT_COMPLETE
int relayClose() {

  digitalWrite(RELAY_PIN, LOW);
  delay(DELAY_STABLE);

  return EXIT_COMPLETE;
  
}

// toggles the status of the Arduino LED 
// ARGUMENTS: the global variable LED_ARDUINO_STATUS (boolean to indicate status of LED)
// RETURNS: EXIT_COMPLETE
int LEDswitch(){

  digitalWrite(LED_BUILTIN, HIGH);

  delay(DELAY_STABLE);

  digitalWrite(LED_BUILTIN, LOW);
  
  return EXIT_COMPLETE;
  
}

