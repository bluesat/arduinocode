// define POT low level functions here

//*********************************************************************************
// FUNCTION DECLARATION
//*********************************************************************************

int setPotResistance(byte addr, byte res, byte memAddress); // set pot resistance
int setCurrentLimit(byte addr, double limit, byte memAddress); // set current limit to current sense amplifier (through pot)
double readPotResistance(byte addr, byte memAddress); // measures the set resistance of the pot

//*********************************************************************************
// FUNCTIONS
//*********************************************************************************

// PURPOSE: set the access control register to the I2C potentiometer
// ARGUMENTS: 
// RETURN VALUE: 

// PURPOSE: write to and reads from a non-volatile memory of I2C potentiometer
// ARGUMENTS: 
// RETURN VALUE:

// PURPOSE: set the resistances of the I2C potentiometer
// ARGUMENTS: address of pin, res to be set (0-127) and memAddress of the circuit to set pot for
// RETURN VALUE: returns an error integer (1 indicates successful setting of data)
// START condition -> identification byte (i2c address) -> address byte (memory address) -> data byte -> STOP condition (ACK after each)
int setPotResistance(byte addr, int res, byte memAddress) {
  
  // check the resistance values to ensure they lie within the min/max values of potentiometer
  if(res > RMAX || res < RMIN) {
    Serial.println(F("ERROR - Resistance too large or too low"));
    return EXIT_ERROR;
  }

  i2c_start(addr|I2C_WRITE); // sends identification byte

  Serial.print(memAddress, BIN);

  // send memory address byte
  if(!i2c_write(memAddress)) {
    Serial.println(F("ERROR - No ACK bit received after memory address sent to POT - Possible invalid memory address"));
    i2c_stop(); // release the bus
    return EXIT_ERROR;
  }

  // send resistance setting byte
  if(!i2c_write(res)) {
    Serial.println(F("ERROR - No ACK bit received after resistance sent to POT"));
    i2c_stop(); // release the bus
    return EXIT_ERROR;    
  }

  i2c_stop(); // release the bus

  if(DEBUG_VERBOSE){
    Serial.print(F("Internal resistance set to (binary): "));
    Serial.println(res, BIN);
    Serial.print(F("Internal resistance set to (integer 0-127): "));
    Serial.println(res, DEC);
  }

  return EXIT_COMPLETE;
  
}

// PURPOSE: read the current resistance of the I2C potentiometer
// ARGUMENTS:  address of pin and memAddress of the circuit to read set pot for
// RETURN VALUE: float of resistance
double readPotResistance(byte addr, byte memAddress){

  uint8_t resistanceByte = 0;
  double resistance = 0;
  
  i2c_start(addr|I2C_WRITE); // sends identification byte

  // send memory address byte
  if(!i2c_write(memAddress)) {
    Serial.println(F("ERROR - No ACK bit received after memory address sent to POT - Possible invalid memory address"));
    i2c_stop(); // release the bus
    return EXIT_ERROR;
  }

  i2c_rep_start(addr|I2C_READ);

  resistanceByte = i2c_read(NACK);

  i2c_stop(); // release the bus

  resistance = double(resistanceByte)*RES_POT/(MAX_RES_BYTE*KILO);

  if(DEBUG_VERBOSE){
    Serial.print(F("Resistance is (integer 0-127): "));
    Serial.println(resistanceByte, DEC);
    Serial.print(F("Resitance is (in kOhms): "));
    Serial.println(resistance);
  }

  return resistance;

}

// PURPOSE: set a current limit for the LT6108 by setting the potentiometer as required
// ARGUMENTS: addr is address of pot, limit is maximum current in amps and memAddress is the register for specific resistor
// RETURN VALUE: returns an error integer (1 indicates successful setting of data)
// see page 18 of http://cds.linear.com/docs/en/datasheet/610812fa.pdf for equations
int setCurrentLimit(byte addr, double limit, byte memAddress) {

  double Vsense; // voltage over the sense resistor
  double Rout; // upper resistance of potentiometer (sets the 
  int resistanceSetting; // resistance as value from 0-127

  if(DEBUG_VERBOSE){
    Serial.print(F("Attempting to set current limit (A): "));
    Serial.println(limit, 4);
  }
  
  Vsense = limit*RES_SENSE;
  Rout = RES_IN*VOLTAGE_REFERENCE/Vsense;

  if(DEBUG_VERBOSE){
    Serial.print(F("Setting resistance in potentiometer (Ohms): "));
    Serial.println(Rout, 4);
  }
  
  resistanceSetting = (Rout/RES_POT)*NUM_WIPER_TAPS;

  resistanceSetting = 20;
  
  if(DEBUG_VERBOSE){
    Serial.print(F("Setting resistance in potentiometer (0-127): "));
    Serial.println(resistanceSetting, DEC);
  }
  
  if(setPotResistance(addr, resistanceSetting, memAddress)==EXIT_ERROR) {
    Serial.println(F("ERROR - Attempt to set current limiting resitance failed"));
    return EXIT_ERROR;
  }

  return resistanceSetting;
  
}

