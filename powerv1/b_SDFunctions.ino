// define low level SD functions here

//*********************************************************************************
// FUNCTION DECLARATION
//*********************************************************************************

int writeValue(double value, char prefix[], char suffix[], char fileName[]); // writes value to SD card
int writeString(char text[], char fileName[]); // write string to the SD card

//*********************************************************************************
// FUNCTIONS
//*********************************************************************************

// PURPOSE: writes a value with strings to a defined file on the SD card
// ARGUMENTS: float value to be written, strings for prefix (before the value), suffix (after the value) and filename - even if not used they should be empty strings
// RETURN VALUE: returns EXIT_COMPLETE unless error occured
int writeValue(double value, char prefix[], char suffix[], char fileName[]) {

  File dataFile = SD.open(fileName, FILE_WRITE);

  if (dataFile) {
    dataFile.print(prefix);
    dataFile.print(value);
    dataFile.println(suffix);
  }
  else {
    if (DEBUG_VERBOSE) {
      Serial.print(fileName);
      Serial.println(F(" - File does not exist for writing to"));
    }
    dataFile.close();
    return EXIT_ERROR;
  }

  dataFile.close();
  return EXIT_COMPLETE;
  
}

// helper function does all non-copy code with a flag/switch case

// PURPOSE: writes a string to the defined file on the SD card
// ARGUMENTS: string of text to be written and filename
// RETURN VALUE: returns EXIT_COMPLETE unless error occured
int writeString(char text[], char fileName[]) {

  File dataFile = SD.open(fileName, FILE_WRITE);

  if (dataFile) {
    dataFile.println(text);
  }
  else {
    if (DEBUG_VERBOSE) {
      Serial.print(fileName);
      Serial.println(F(" - File does not exist for writing to"));
    }
    dataFile.close();
    return EXIT_ERROR;
  }

  dataFile.close();
  return EXIT_COMPLETE;
  
}

