// define high level function (in terms of circuits not addresses here)

//*********************************************************************************
// FUNCTION DECLARATION
//*********************************************************************************

int checkInputsTest(char circuit, char voltage); // checks the inputs to the test functions
float currentRead(char circuit, char voltage); // high level current read function
int IOSet(char circuit); // high level IO set - (uses globally defined IO variables)
int currentLimitSet(char circuit, char voltage, float currentLimit); // high level current limit set function
int resetCurrentLatch(char circuit, char voltage); // high level current latch reset function
double getCurrent(char circuit, char voltage, int storeStatus); // high level current read function (fixed)
double getResistance(char circuit, char voltage, int storeStatus); // high level resistance read function
double getVoltage(char voltage, int storeStatus); // high level voltage read function

//*********************************************************************************
// FUNCTIONS
//*********************************************************************************


// PURPOSE: Checks the inputs for the test functions
// ARGUMENTS: char for the designation of the circuit (1/2) and voltage for the specific voltage to latch (3/5)
// RETURNS: EXIT_COMPLETE (1) if no issues and EXIT_ERROR if unsatifactory (-2)
int checkInputsTest(char circuit, char voltage){

  if(voltage!='5'&&voltage!='3'){
    Serial.println(F("Please enter a 3 or 5 as the voltage character"));
  }

  if(circuit!='1'&&circuit!='2'){
    Serial.println(F("Please enter a 1 or 2 as the circuit"));
  }

  return EXIT_COMPLETE;

}

// PURPOSE: function that reduces the current limit of a line so as to force a latch shut
// ARGUMENTS: char for the designation of the circuit (1/2) and voltage for the specific voltage to latch (3/5)
// RETURNS: float of the current
float currentRead(char circuit, char voltage){

  if(checkInputsTest(circuit, voltage)!=EXIT_COMPLETE){
    Serial.println(F("Inputs are invalid"));
    return EXIT_ERROR;
  }

  if (circuit=='1') {
    if (voltage=='3') {
      return convertCurrent(readCurrent(I2C_ADC_ADDR_1, 0));
    }
    else if (voltage == '5') {
      return convertCurrent(readCurrent(I2C_ADC_ADDR_1, 0));
    }
    else {
      return EXIT_ERROR;
    }
  }
  else if (circuit=='2') {
    if (voltage=='3') {
      return convertCurrent(readCurrent(I2C_ADC_ADDR_2, 0));
    }
    else if (voltage == '5') {
      return convertCurrent(readCurrent(I2C_ADC_ADDR_2, 0));
    }
    else {
      return EXIT_ERROR;
    }
  }
  else {
    return EXIT_ERROR;
  }
  
}

// PURPOSE: function that switches a specific line through IO control
// ARGUMENTS: char for the designation of the circuit (1/2) and voltage for the specific voltage to latch (3/5)
// RETURNS: exit status
int IOLineSwitch(char circuit, char voltage) {

  if(checkInputsTest(circuit, voltage)!=EXIT_COMPLETE){
    Serial.println(F("Inputs are invalid"));
    return EXIT_ERROR;
  }

  // changes the single global variable that defines the line control of line as specified by input arguments
  if (circuit=='1') {
    if (voltage=='3') {
      IO_3_EN_1 = !IO_3_EN_1;
    }
    else if (voltage == '5') {
      IO_5_EN_1 = !IO_5_EN_1;
    }
    else {
      return EXIT_ERROR;
    }
  }
  else if (circuit=='2') {
    if (voltage=='3') {
      IO_3_EN_2 = !IO_3_EN_2;
    }
    else if (voltage == '5') {
      IO_5_EN_2 = !IO_5_EN_2;
    }
    else {
      return EXIT_ERROR;
    }
  }
  else {
    return EXIT_ERROR;
  }

  return IOSet(circuit);

}

// PURPOSE: high level to set the IO expanders output
// ARGUMENTS: char for the designation of the circuit (1/2) (4x booleans for the output settings are globally defined)
// RETURNS: exit status
int IOSet(char circuit) {

  if (circuit == '1') {
      return setIOOutputs(I2C_POT_ADDR_1, IO_5_EN_1, IO_5_RESET_1, IO_3_EN_1, IO_3_RESET_1);
  }
  else if (circuit == '2') {
      return setIOOutputs(I2C_POT_ADDR_2, IO_5_EN_2, IO_5_RESET_2, IO_3_EN_2, IO_3_RESET_2);
  }
  else {
    return EXIT_ERROR;
  }
  
}

// PURPOSE: high level to set the current limit on a specific path
// ARGUMENTS: char for the designation of the circuit (1/2), voltage for the specific voltage to latch (3/5) and float as the current limit in Amperes
// RETURNS: EXIT_COMPLETE
int currentLimitSet(char circuit, char voltage, float currentLimit){

  if(checkInputsTest(circuit, voltage)!=EXIT_COMPLETE){
    Serial.println(F("Inputs are invalid"));
    return EXIT_ERROR;
  }

  if (circuit == '1') {
    if (voltage == '3') {
      return setCurrentLimit(I2C_POT_ADDR_1, currentLimit, MEM_POT_3);
    }
    else if (voltage == '5') {
      return setCurrentLimit(I2C_POT_ADDR_1, currentLimit, MEM_POT_5);
    }
    else {
      return EXIT_ERROR;
    }
  }
  else if (circuit == '2') {
    if (voltage == '3') {
      return setCurrentLimit(I2C_POT_ADDR_2, currentLimit, MEM_POT_3);
    }
    else if (voltage == '5') {
      return setCurrentLimit(I2C_POT_ADDR_2, currentLimit, MEM_POT_5);
    }
    else {
      return EXIT_ERROR;
    }
  }
  else {
    return EXIT_ERROR;
  }

  
}

// PURPOSE: function that resets a latched line
// ARGUMENTS: char for the designation of the circuit (1/2) and voltage for the specific voltage to latch (3/5)
// RETURNS: exit status
int resetCurrentLatch(char circuit, char voltage){

  if(checkInputsTest(circuit, voltage)!=EXIT_COMPLETE){
    Serial.println(F("Inputs are invalid"));
    return EXIT_ERROR;
  }

  if (circuit == '1') {
    if (voltage == '3') {
      return resetIOCurrentSenseAmp(I2C_IO_ADDR_1, PIN_3_RESET);
    }
    else if (voltage == '5') {
      return resetIOCurrentSenseAmp(I2C_IO_ADDR_1, PIN_5_RESET);
    }
    else {
      return EXIT_ERROR;
    }
  }
  else if (circuit == '2') {
    if (voltage == '3') {
      return resetIOCurrentSenseAmp(I2C_IO_ADDR_2, PIN_3_RESET);
    }
    else if (voltage == '5') {
      return resetIOCurrentSenseAmp(I2C_IO_ADDR_2, PIN_5_RESET);
    }
    else {
      return EXIT_ERROR;
    }
  }
  else {
    return EXIT_ERROR;
  }

  return EXIT_COMPLETE;
}

// PURPOSE: function that reads the current through the ADC
// ARGUMENTS: char for the designation of the circuit (1/2) and voltage for the specific voltage to latch (3/5)
// RETURNS: double of the current through the sense resistor
double getCurrent(char circuit, char voltage, int storeStatus){
    
  int currentRaw; // pure ADC voltage output in integer form
  double current; // converted ADC value into current using resistor relationships from LT6108 datasheet http://cds.linear.com/docs/en/datasheet/610812fa.pdf
  
  if(checkInputsTest(circuit, voltage)!=EXIT_COMPLETE){
    Serial.println(F("Inputs are invalid"));
    return EXIT_ERROR;
  }

  if (circuit == '1') {
    if (voltage == '3') {
      currentRaw = readCurrent(I2C_ADC_ADDR_1, CHANNEL_3V3);
    }
    else if (voltage == '5') {
      currentRaw = readCurrent(I2C_ADC_ADDR_2, CHANNEL_5V);
    }
    else {
      return EXIT_ERROR;
    }
  }
  else if (circuit == '2') {
    if (voltage == '3') {
      currentRaw = readCurrent(I2C_ADC_ADDR_2, CHANNEL_3V3);
    }
    else if (voltage == '5') {
      currentRaw = readCurrent(I2C_ADC_ADDR_2, CHANNEL_5V);
    }
    else {
      return EXIT_ERROR;
    }
  }
  else {
    return EXIT_ERROR;
  }

  current = convertCurrent(currentRaw);

  if(storeStatus == WRITE) {
    char prefix [] = {circuit, ' ', voltage, 'V', ' ', NULL};
    writeValue(current, prefix, "A", FILENAME);
  }
  else if (storeStatus != NO_WRITE) {
    return EXIT_ERROR;
  }

  return current;

}

// PURPOSE: function that gets the set resistance of the pot
// ARGUMENTS: char for the designation of the circuit (1/2) and voltage for the specific voltage pot acts on (3/5) and integer for writing to SD (1 = write, 0 = not)
// RETURNS: double of the resistance in kOhms in pot
double getResistance(char circuit, char voltage, int storeStatus){

  double resistance = 0;
  
  if(checkInputsTest(circuit, voltage)!=EXIT_COMPLETE){
    Serial.println(F("Inputs are invalid"));
    return EXIT_ERROR;
  }

  if (circuit == '1') {
    if (voltage == '3') {
      resistance = readPotResistance(I2C_POT_ADDR_1, MEM_POT_3);
    }
    else if (voltage == '5') {
      resistance = readPotResistance(I2C_POT_ADDR_1, MEM_POT_5);
    }
    else {
      return EXIT_ERROR;
    }
  }
  else if (circuit == '2') {
    if (voltage == '3') {
      resistance = readPotResistance(I2C_POT_ADDR_2, MEM_POT_3);
    }
    else if (voltage == '5') {
      resistance = readPotResistance(I2C_POT_ADDR_2, MEM_POT_5);
    }
    else {
      return EXIT_ERROR;
    }
  }
  else {
    return EXIT_ERROR;
  }

  if(storeStatus == WRITE) {
    char prefix [] = {circuit, ' ', voltage, 'V', ' ', NULL};
    writeValue(resistance, prefix, "A", FILENAME);
  }
  else if (storeStatus != NO_WRITE) {
    return EXIT_ERROR;
  }

  return resistance;

}

// PURPOSE: function that reads the voltage through the ADC on the Arduino
// ARGUMENTS: char for the designation of the circuit (1/2) and voltage for the specific voltage to latch (3/5)
// RETURNS: double of the current through the sense resistor
double getVoltage(char voltage, int storeStatus){
    
  int voltageRaw; // pure ADC voltage output in integer form
  double voltageConv; // converted ADC value into actual voltage (1023 = 5V)

  // uses a '1' for the circuit to pass the checkInputsTest
  if(checkInputsTest('1', voltage)!=EXIT_COMPLETE){
    Serial.println(F("Inputs are invalid"));
    return EXIT_ERROR;
  }

  if (voltage == '3') {
    voltageRaw = analogRead(PIN_ARD_5V);
  }
  else if (voltage == '5') {
    voltageRaw = analogRead(PIN_ARD_3V3);
  }
  else {
    return EXIT_ERROR;
  }
  
  voltageConv = VOLTAGE_ARD*voltageRaw/1023.0;

  if(storeStatus == WRITE) {
    char prefix [] = {voltage, 'V', ' ', 'R', 'a', 'i', 'l', ' ', NULL};
    writeValue(voltageConv, prefix, "A", FILENAME);
  }
  else if (storeStatus != NO_WRITE) {
    return EXIT_ERROR;
  }

  return voltageConv;

}
