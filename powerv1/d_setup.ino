// define setup functions here

//*********************************************************************************
// FUNCTION DECLARATION
//*********************************************************************************

int setupIO(char circuit); // sets all pins as output, sets reset/shutdown pins and resets latches
int setupPot(char circuit); // sets pots to default current limit
int setupPot(char circuit); // sets pots to default current limit
int setupOpeningText(); // prints some general text before the program starts to the serial monitor
int printBSLogo(); // prints the BS logo
int SDCheckVol(boolean LIST_FILES); // checks the SD card for files onboard
int LEDsetup(); // flashes the LED 3 times
int sd_init(); // initialises the SD card

//*********************************************************************************
// FUNCTIONS
//*********************************************************************************

// PURPOSE: sets up the IO expanders over I2C
// ARGUMENTS: char circuit (usually number on board designator) referring to it
// RETURN: EXIT_COMPLETE if successful (1) and EXIT_ERROR if not (-2)
int setupIO(char circuit) {

  byte addr;

  if (circuit == '1') {
    addr = I2C_IO_ADDR_1;
  }
  else if (circuit == '2') {
    addr = I2C_IO_ADDR_2;
  }
  else {
    return EXIT_ERROR;
  }

  Serial.print(F("IO "));
  Serial.print(circuit);
  Serial.println(F(" - Setting up ..."));

  // sets all IO pins as outputs
  Serial.println(F("Setting pins as output"));
  if(setIOChannels(addr)!=EXIT_COMPLETE){
    Serial.println(F("Unable to setup IO outputs"));
    return EXIT_ERROR;
  }

// set all outputs as open
  Serial.println(F("Setting shutdown/reset pins"));
  if(IOSet(circuit)!=EXIT_COMPLETE){
    Serial.println(F("Unable to setup shutdown/reset pins"));
    return EXIT_ERROR;
  } 

  // resets all CSA latches to be open
  Serial.println(F("Resetting latches on current sense amplifier"));
  delay(DELAY_PRINT);
  resetIOCurrentSenseAmp(addr, PIN_5_RESET);\
  delay(DELAY_PRINT);
  resetIOCurrentSenseAmp(addr, PIN_3_RESET);

  Serial.print(F("IO "));
  Serial.print(circuit);
  Serial.println(F(" - Setup complete"));
  
  return EXIT_COMPLETE;

}


// PURPOSE: sets up the ADCs over I2C
// ARGUMENTS: char circuit (usually number on board designator) referring to it
// RETURN: EXIT_COMPLETE if successful (1) and EXIT_ERROR if not (-2)
int setupADC(char circuit) {

  byte addr;

  if (circuit == '1') {
    addr = I2C_ADC_ADDR_1;
  }
  else if (circuit == '2') {
    addr = I2C_ADC_ADDR_2;
  }
  else {
    return EXIT_ERROR;
  }

  Serial.print(F("ADC "));
  Serial.print(circuit);
  Serial.println(F(" - Setting up ..."));

  // sets ADC measurement mode and sleep settings
  Serial.println(F("Setting ADC modes (5V reading by default)"));
  if(setupADC(addr, CHANNEL_5V, SLEEP_MODE_OFF)!=EXIT_COMPLETE){
    Serial.println(F("Unable to setup ADC"));
    return EXIT_ERROR;
  }

  Serial.print(F("ADC "));
  Serial.print(circuit);
  Serial.println(F(" - Setup complete"));
  
  return EXIT_COMPLETE;

}

// PURPOSE: sets up the potentiometers over I2C
// ARGUMENTS: char circuit (usually number on board designator) referring to it
// RETURN: EXIT_COMPLETE if successful (1) and EXIT_ERROR if not (-2)
int setupPot(char circuit) {

  // future variables to use resistance returned - currently not implemented
  int resistanceSetting3;
  int resistanceSetting5;

  Serial.print(F("Potentiometer "));
  Serial.print(circuit);
  Serial.println(F(" - Setting up ..."));

  Serial.println(F("Initial pot reading (in kOhms) is: "));
  getResistance(circuit, '3', NO_WRITE);
  Serial.println(F("Setting 3V3 current limit"));
  resistanceSetting3 = currentLimitSet(circuit, '3', CURRENT_LIMIT_DEFAULT);
  Serial.println(F("Final pot reading (in kOhms) is: "));
  getResistance(circuit, '3', NO_WRITE);

  if(resistanceSetting3==EXIT_ERROR){
    Serial.println(F("3V3 current limit failed to set"));
    return EXIT_ERROR;
  }

  Serial.println(F("Initial pot reading (in kOhms) is: "));
  getResistance(circuit, '5', NO_WRITE);
  Serial.println(F("Setting 5V current limit"));
  resistanceSetting5 = currentLimitSet(circuit, '5', CURRENT_LIMIT_DEFAULT);
  Serial.println(F("Final pot reading (in kOhms) is: "));  
  getResistance(circuit, '5', NO_WRITE);
  
  if(resistanceSetting5==EXIT_ERROR){
    Serial.println(F("5V current limit failed to set"));
    return EXIT_ERROR;
  }

  Serial.print(F("Potentiometer "));
  Serial.print(circuit);
  Serial.println(F(" - Set up\n"));

  return EXIT_COMPLETE;

}

// PURPOSE: prints some general information about the program to the serial monitor before running the main program
// ARGUMENTS: none
// RETURN: EXIT_COMPLETE
int setupOpeningText(){

  Serial.println(F("BLUEsat - Control Board Progam (testing and operation)"));
  Serial.println(F("University of New South Wales"));
  Serial.println(F("See https://www.facebook.com/bluesat.unsw/?ref=br_rs for more information\n"));

  printBSLogo();
  Serial.println(F("Written by Declan Walsh"));

  return EXIT_COMPLETE;

}

// PURPOSE: initialises the SD card
// ARGUMENTS: none
// RETURN: EXIT_COMPLETE
int sd_init(){

  if (!SD.begin(CS_PIN)) {
    Serial.println("initialization failed!");
    return EXIT_ERROR;
  }

  if (!card.init(SPI_HALF_SPEED, 10)) {
    Serial.println(F("Initialization failed. Check wiring and connction"));
    return EXIT_ERROR;
  } else {
    Serial.println(F("Wiring is correct and a card is present."));
  }

  // Now we will try to open the 'volume'/'partition' - it should be FAT16 or FAT32
  if (!volume.init(card)) {
    Serial.println(F("Could not find FAT16/FAT32 partition.\nMake sure you've formatted the card"));
    return EXIT_ERROR;
  }

  return EXIT_COMPLETE;
  
}

// PURPOSE: prints a list of the files on the volume
// ARGUMENTS: none
// RETURN: EXIT_COMPLETE
int SDCheckVol(boolean LIST_FILES) {

  root.openRoot(volume);
  // list all files in the card with date and size
  if(LIST_FILES){
    root.ls(LS_R | LS_DATE | LS_SIZE);
  }

  return EXIT_COMPLETE;
  
}

// PURPOSE: flickers the LED on the Arduino 3 times to indicate setup has completed
// ARGUMENTS: none
// RETURN: EXIT_COMPLETE
int LEDsetup() {

  for(int i = 0; i < 3; i++) {
    Serial.println("SETUP - Complete");
    digitalWrite(LED_BUILTIN,HIGH);
    delay(DELAY_LED);
    digitalWrite(LED_BUILTIN, LOW);
  }

  return EXIT_COMPLETE;
  
}

// PURPOSE: prints the BS logo to serial
// ARGUMENTS: none
// RETURNS: EXIT_COMPLETE
int printBSLogo() {

  Serial.println(F("      ........................        \n        ...    ...MMMM..........     .  \n        ...    MMMMMMMMMZ.......     .  \n        ... MMMMMMN..MMMMMM8............\n        .7MMMMMM.......:MMMMMM..        \n.......MMMMMM+       .  . MMMMMMM.......\n....MMMMMM:.            . .  8MMMMMM....\n..MMMMMM. .     .  . ...... ....MMMMMM..\n..MMMMMM        .  . ...... ... MMMMMM..\n..MMM.MMMMM    .     .  .   .MMMMMMMMM..\n..MMM.  +MMMM?  .  .  .. .MMMMM ..MMMM..\n..MMM.    .MMMMM.......~MMMM7 ....MMMM..\n..MMM.  . .. .NMMMM~?MMMMI........MMMM..\n..MMM.          .MMMMMM  .........MMMM..\n..MMM.          . ~MM... .........MMMM..\n..MMM.          . ~MM... .........MMMM..\n..MMM.    ..  . ..~MM... .........MMMM..\n..MMM           ..~MM... .........MMMM..\n..MMM.     .  . . ~MM... .........MMMM..\n..MMMMM ....... ..~MM... ........MMMMM..\n...MMMMMM+..  . . ~MM... .....MMMMMMM ..\n.... =MMMMMM: ....~MM......IMMMMMM......\n.....   ?MMMMMM ..~MM ..~MMMMMM  .......\n....      .MMMMMMN~MM.MMMMMM7...........\n....    . .. .MMMMMMMMMMMN..............\n....    ....... .MMMMMM  ...............\n....    . .   . . .~:... ...............\n....    . ..    . ...... ...............\n"));

  return EXIT_COMPLETE;
}
