// define testing functions here

//*********************************************************************************
// FUNCTION DECLARATION
//*********************************************************************************

int currentTestLatch(char circuit, char voltage); // tests the overcurrent latch
int manualTestLatch(char circuit, char voltage); // tests manual closing of line
int relayTest(int storeStatus); // tests manual closing of relay

//*********************************************************************************
// FUNCTIONS
//*********************************************************************************

// PURPOSE: function that reduces the current limit of a line so as to force a latch shut
// ARGUMENTS: char for the designation of the circuit (1/2) and voltage for the specific voltage to latch (3/5)
// RETURNS: EXIT_COMPLETE
int currentTestLatch(char circuit, char voltage){

  float currentLatch;

  if(checkInputsTest(circuit, voltage)!=EXIT_COMPLETE){
    Serial.println(F("Inputs are invalid"));
    return EXIT_ERROR;
  }

  Serial.println(F("Beginning current trip test..."));


  // reduce the current limit to trip the overcurrent
  Serial.println(F("\nSetting line to low current limit..."));
  currentLimitSet(circuit, voltage, CURRENT_LIMIT_TEST_LOW);
  delay(DELAY_TEST);
  Serial.println(F("\nLine set to low current limit"));
  delay(DELAY_STABLE);
  // measure the current through to ensure the line has been closed
  Serial.println(F("Measuring current on rail"));
  currentLatch = currentRead(circuit, voltage);
  Serial.print(F("Current is (A): "));
  Serial.println(currentLatch, 3); // read the current from the analog-digital converter


  // increase the current limit again
  Serial.println(F("Resetting the current limit..."));
  currentLimitSet(circuit, voltage, CURRENT_LIMIT_DEFAULT);
  delay(DELAY_TEST);
  Serial.println(F("\nLine set to default current limit"));
  delay(DELAY_STABLE);
  // measure the current through to ensure the latch has been tripped and remains shut despite increasing limit
  Serial.println(F("Measuring current on rail"));
  currentLatch = currentRead(circuit, voltage);
  Serial.print(F("Current is (A): "));
  Serial.println(currentLatch, 3); // read the current from the analog-digital converter

  // resets the latch
  Serial.println(F("Resetting the latch..."));
  resetCurrentLatch(circuit, voltage);
  Serial.println("\nLatch reset");
  delay(DELAY_STABLE);
  Serial.print("Measuring current on rail");
  currentLatch = currentRead(circuit, voltage);
  Serial.println(F("Current is (A): "));
  Serial.println(currentLatch, 3); // read the current from the analog-digital converter

  Serial.println(F("Current trip test completed"));

  return EXIT_COMPLETE;

}

// PURPOSE: function that manually closes a line to test if it holds shut
// ARGUMENTS: char for the designation of the circuit (1/2) and voltage for the specific voltage to latch (3/5)
// RETURNS: EXIT_COMPLETE
int manualTestLatch(char circuit, char voltage){

  float currentLatch;

  if(checkInputsTest(circuit, voltage)!=EXIT_COMPLETE){
    Serial.println(F("Inputs are invalid"));
    return EXIT_ERROR;
  }

  Serial.println(F("Beginning manual latch test..."));

  IOLineSwitch(circuit, voltage);

  // reduce the current limit to trip the overcurrent
  Serial.println(F("\nClosing the line manually..."));
  currentLimitSet(circuit, voltage, CURRENT_LIMIT_TEST_LOW);
  delay(DELAY_TEST);
  Serial.println(F("\nLine set to low current limit"));
  delay(DELAY_STABLE);
  // measure the current through to ensure the line has been closed
  Serial.println(F("Measuring current on rail"));
  currentLatch = currentRead(circuit, voltage);
  Serial.print(F("Current is (A): "));
  Serial.println(currentLatch, 3); // read the current from the analog-digital converter

  IOLineSwitch(circuit, voltage);

  // resets the latch
  Serial.println(F("Opening the line manually..."));
  resetCurrentLatch(circuit, voltage);
  Serial.println("\nLatch reset");
  delay(DELAY_STABLE);
  Serial.println("Measuring current on rail");
  currentLatch = currentRead(circuit, voltage);
  Serial.print(F("Current is (A): "));
  Serial.println(currentLatch, 3); // read the current from the analog-digital converter

  Serial.println(F("Manual latch test completed"));

  return EXIT_COMPLETE;

}


// PURPOSE: function that manually closes the relay to test boot
// ARGUMENTS: writeResults is boolean to store the results to the SD card
// RETURNS: EXIT_COMPLETE
int relayTest(int storeStatus){

  Serial.println(F("Testing relay..."));

  Serial.println(F("Closing relay..."));
  relayClose();
  delay(DELAY_TEST);

  if(storeStatus == WRITE){
    writeString("RELAY CLOSED", FILENAME);
  }
  else if (storeStatus != NO_WRITE) {
    return EXIT_ERROR;
  }

  Serial.println(getVoltage('3', storeStatus));
  Serial.println(getVoltage('5', storeStatus));

  Serial.println(F("Opening relay..."));
  relayOpen();
  delay(DELAY_TEST);

  if(storeStatus == WRITE){
    writeString("RELAY CLOSED", FILENAME);
  }
  else if (storeStatus != NO_WRITE) {
    return EXIT_ERROR;
  }

  Serial.println(getVoltage('3', storeStatus));
  Serial.println(getVoltage('5', storeStatus));
  
  return EXIT_COMPLETE;

}
