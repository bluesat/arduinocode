//*********************************************************************************
// SETUP
//*********************************************************************************

void setup() {

  Serial.begin(9600);

  //setupOpeningText();

  Serial.println(F("Starting Program..."));

  // ensures relay is closed to allow time for caps to charge
  relayClose();
  delay(DELAY_CAP);
  
  // opens the relay (needs to be done first to allow complete boot before using I2C)
  pinMode(RELAY_PIN, OUTPUT);
  relayOpen();

  Serial.println(F("Initializing SD card..."));
  sd_init();
  SDCheckVol(false);

  if(WRITE_STATUS == WRITE){
    writeString("NEW RUN - SD Booted", FILENAME);
  }
  
  Serial.println(F("Setting up I2C bus..."));
  if(!i2c_init()) {
    Serial.println(F("Initilisation failed - bus locked up"));
  }

  delay(DELAY_I2C);

  Serial.println(F("SETUP - Setting up potentiometers"));
  setupPot('1');
  Serial.println(F("\nSETUP - Potentiometer setup complete\n"));

  Serial.println(F("SETUP - Setting up IO expanders\n"));
  setupIO('1');
  Serial.println(F("\nSETUP - IO expanders setup complete\n"));

  Serial.println(F("SETUP - Setting up ADCs\n"));
  setupADC('1');
  Serial.println(F("\nSETUP - ADC setup complete\n"));

  pinMode(LED_BUILTIN, OUTPUT); // define LED pin to be driven
  LEDsetup(); // flicker Arduino LED slowly to indiciate boot complete
 
  Serial.println(F("SETUP - Complete\n\n"));

  if(WRITE_STATUS == WRITE){
    writeString("SETUP - Complete", FILENAME);
  }
}

//*********************************************************************************
// LOOP
//*********************************************************************************

void loop() {

  Serial.print(F("Cycle #: "));
  Serial.println(CYCLE_COUNTER);

  if(WRITE_STATUS == WRITE){
    writeValue((double) CYCLE_COUNTER, "Cycle Number: ", " - Start", FILENAME);
  }
  
  LEDswitch();
  
  //**************************
  // Overcurrent trip testing
  //**************************

  //currentTestLatch('1', '3');
  //currentTestLatch('1', '5');
  
  //**************************
  // Manual closing setting
  //**************************

  // manualTestLatch('1', '3');
  // manualTestLatch('1', '5');

  //**************************
  // Relay test
  //**************************

  relayTest(WRITE_STATUS);

  //**************************
  // Current Measurements
  //**************************

  Serial.println(F("Circuit 1 3V3 - Current"));
  Serial.println(getCurrent('1', '3', WRITE_STATUS));
  Serial.println(F("Circuit 1 5V - Current"));
  Serial.println(getCurrent('1', '5', WRITE_STATUS));

  //**************************
  // Voltage Measurements
  //**************************

  Serial.println(F("Circuit 1 3V3 - Voltage"));
  Serial.println(getVoltage('3', WRITE_STATUS));
  Serial.println(F("Circuit 1 5V - Voltage"));
  Serial.println(getVoltage('5', WRITE_STATUS));
  
  Serial.println(F("Loop completed\n\n"));

  if(WRITE_STATUS == WRITE){
    writeValue((double) CYCLE_COUNTER, "Cycle Number: ", " - End", FILENAME);
  }
  
  CYCLE_COUNTER = CYCLE_COUNTER + 1;

  if(WRITE_STATUS == WRITE){
    writeString("LOOP - Complete", FILENAME);
  }
  delay(DELAY_END); // pause before reperforming measurements

}
