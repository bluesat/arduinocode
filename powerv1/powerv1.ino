// BlueSat Control Circuit Testing Code
// Program to control the power regulation and supply board v1
// Author: Declan Walsh
// Last Modified: 22/7/2017

// Compilation Notes:
// SoftI2C Library must be downloaded from https://playground.arduino.cc/Main/SoftwareI2CLibrary and installed
// SD.h library must be edited to remove all references to SCL_PIN and SDA_PIN in SD2PinMap.h

// Update Log:
// 1 - Added list of devices and memory locations
// 2 - Added ADC measurement function to allow current measurement
// 3 - Added the potentiometer resistance setting function
// 4 - Added IO initial setup and pin manipulation
// 5 - Tested IO expander/ADC/pot communication successfully
// 6 - Tested potentiometer setting successfullyf
// 7 - IO options grouped together (replaced ^ with <<)
// 8 - Tested line shutdown successfully
// 9 - Tested current measurement successfully (approx. 10% off from actual measured value)
// 10 - Added current limit function
// 11 - Determined the current limit error was due to use of a non-latching comparator
// 12 - Added 3V Rail Test to Main Loop
// 13 - Moved program into multiple tabs (compiles first tab first and then the rest alphabetically)
// 14 - Made functions able to use different addresses
// 15 - Made all strings that are being printed flash based to free up dynamic memory, created supplementary setup functions to tidy up core setup function, added a testing set of functions and created higher level functions to interface on a board level in terms of designators
// 16 - Fixed I2C addresses for current board
// 17 - Fixed ADC voltage level (uses REFCOMP), added config functions for ADC and added pot reading functions
// 18 - Added SD card library functionality, relay control
// 19 - Added SD write function and added writing to loops
// 20 - Updated MEM_POT_3 and MEM_POT_5
// 21 - Removed string type (causes errors), added some more writes and finalised code (FLIGHT CODE - v1)

// To Do:
// Add wakeup time limit to ADC waking (currently skips the 200ms wait)
// Add setup of access control register to the pot
// Add function that prints out the current IO settings
// Move more low level information into the debugging case
// Possible issue with setup order as the latches are opened before the current limit is properly set - may lead to overcurrent before detecting it - possibly investigate reworking this
// Possible issue with setup functions where they exit with an error if they can't perform an action - this may prevent further setup from taking place causing more damage than would otherwise occur

// Long Term To Do:
// Create a sleep mode for the system where all lines are closed and components go into a sleep modes (ultra low power consumption)
